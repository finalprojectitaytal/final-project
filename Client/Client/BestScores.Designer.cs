﻿namespace Client
{
    partial class BestScores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.first = new System.Windows.Forms.Label();
            this.second = new System.Windows.Forms.Label();
            this.third = new System.Windows.Forms.Label();
            this.Back = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // first
            // 
            this.first.AutoSize = true;
            this.first.BackColor = System.Drawing.Color.Transparent;
            this.first.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.first.ForeColor = System.Drawing.Color.White;
            this.first.Location = new System.Drawing.Point(251, 151);
            this.first.Name = "first";
            this.first.Size = new System.Drawing.Size(48, 19);
            this.first.TabIndex = 1;
            this.first.Text = "label1";
            // 
            // second
            // 
            this.second.AutoSize = true;
            this.second.BackColor = System.Drawing.Color.Transparent;
            this.second.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.second.ForeColor = System.Drawing.Color.White;
            this.second.Location = new System.Drawing.Point(251, 188);
            this.second.Name = "second";
            this.second.Size = new System.Drawing.Size(48, 19);
            this.second.TabIndex = 2;
            this.second.Text = "label1";
            // 
            // third
            // 
            this.third.AutoSize = true;
            this.third.BackColor = System.Drawing.Color.Transparent;
            this.third.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.third.ForeColor = System.Drawing.Color.White;
            this.third.Location = new System.Drawing.Point(251, 230);
            this.third.Name = "third";
            this.third.Size = new System.Drawing.Size(48, 19);
            this.third.TabIndex = 3;
            this.third.Text = "label1";
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(638, 21);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(113, 20);
            this.Back.TabIndex = 4;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::Client.Properties.Resources.dd;
            this.button1.Cursor = System.Windows.Forms.Cursors.Default;
            this.button1.Location = new System.Drawing.Point(89, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 28);
            this.button1.TabIndex = 5;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BestScores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.Untitled_2__1_;
            this.ClientSize = new System.Drawing.Size(534, 461);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.third);
            this.Controls.Add(this.second);
            this.Controls.Add(this.first);
            this.Name = "BestScores";
            this.Text = "BestScores";
            this.Load += new System.EventHandler(this.BestScores_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label first;
        private System.Windows.Forms.Label second;
        private System.Windows.Forms.Label third;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Button button1;
    }
}