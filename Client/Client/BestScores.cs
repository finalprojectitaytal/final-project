﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;

namespace Client
{
    public partial class BestScores : Form
    {
        private Form navigate;
        private string data;
        private NetworkStream clientStream;

        public BestScores(Form navigate, string data,NetworkStream clientStream)
        {
            InitializeComponent();

            this.navigate = navigate;
            this.data = data;
            this.clientStream = clientStream;

            GetData();
        }

        public void GetData()
        {
            string dataWithoutCode = data.Substring(3);

            dataWithoutCode = setLabel(dataWithoutCode, first);
            dataWithoutCode = setLabel(dataWithoutCode, second);
            dataWithoutCode = setLabel(dataWithoutCode, third);
        }

        public string setLabel(string data, Label l)
        {
            string user;
            //string two;
            //string three;
            int userLen;
            //int twoLen;
            //int threeLen;
            string numOfCorrectAns;
            int index = -1;
            int i = 0;
            string tempData = data;


            userLen = int.Parse(tempData.Substring(0, 2));
            if (userLen != 0)
            {
                tempData = tempData.Substring(2);

                user = tempData.Substring(0, userLen);

                tempData = tempData.Substring(userLen);

                numOfCorrectAns = tempData.Substring(0, 6);

                tempData = tempData.Substring(6);

                int len = numOfCorrectAns.Length;

                while (i < len)
                {
                    if (numOfCorrectAns[i] == '0')
                    {
                        index = i;
                    }
                    else
                    {
                        i = len;
                    }

                    i++;
                }

                if (index != -1)
                {
                    numOfCorrectAns = numOfCorrectAns.Substring(index + 1);
                }

                l.Text = user + " - " + numOfCorrectAns;
            }

            return tempData;
        }

        private void BestScores_Load(object sender, EventArgs e)
        {

        }

        private void Back_Click(object sender, EventArgs e)
        {
            this.Hide();
            navigate.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();

            this.navigate.Show();

            this.Close();
        }

        private void BestScores_FormClosing(object sender, FormClosingEventArgs e)
        {
            string message = "201";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }
    }
}
