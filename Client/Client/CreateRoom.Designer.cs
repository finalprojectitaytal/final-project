﻿namespace Client
{
    partial class CreateRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.roomName = new System.Windows.Forms.TextBox();
            this.questionsNo = new System.Windows.Forms.TextBox();
            this.questionTime = new System.Windows.Forms.TextBox();
            this.playerNo = new System.Windows.Forms.TextBox();
            this.Send = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.userName = new System.Windows.Forms.Label();
            this.playerNoError = new System.Windows.Forms.Label();
            this.ErrorMsg = new System.Windows.Forms.Label();
            this.category = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // roomName
            // 
            this.roomName.Location = new System.Drawing.Point(281, 164);
            this.roomName.Name = "roomName";
            this.roomName.Size = new System.Drawing.Size(182, 20);
            this.roomName.TabIndex = 4;
            // 
            // questionsNo
            // 
            this.questionsNo.Location = new System.Drawing.Point(281, 212);
            this.questionsNo.Name = "questionsNo";
            this.questionsNo.Size = new System.Drawing.Size(182, 20);
            this.questionsNo.TabIndex = 5;
            // 
            // questionTime
            // 
            this.questionTime.Location = new System.Drawing.Point(281, 311);
            this.questionTime.Name = "questionTime";
            this.questionTime.Size = new System.Drawing.Size(182, 20);
            this.questionTime.TabIndex = 6;
            // 
            // playerNo
            // 
            this.playerNo.Location = new System.Drawing.Point(281, 263);
            this.playerNo.Name = "playerNo";
            this.playerNo.Size = new System.Drawing.Size(182, 20);
            this.playerNo.TabIndex = 7;
            this.playerNo.TextChanged += new System.EventHandler(this.playerNo_TextChanged);
            // 
            // Send
            // 
            this.Send.Location = new System.Drawing.Point(241, 404);
            this.Send.Name = "Send";
            this.Send.Size = new System.Drawing.Size(77, 24);
            this.Send.TabIndex = 8;
            this.Send.Text = "Send";
            this.Send.UseVisualStyleBackColor = true;
            this.Send.Click += new System.EventHandler(this.Send_Click);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(470, 23);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(71, 20);
            this.back.TabIndex = 9;
            this.back.Text = "back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // userName
            // 
            this.userName.AutoSize = true;
            this.userName.Location = new System.Drawing.Point(171, 27);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(35, 13);
            this.userName.TabIndex = 10;
            this.userName.Text = "label5";
            // 
            // playerNoError
            // 
            this.playerNoError.AutoSize = true;
            this.playerNoError.ForeColor = System.Drawing.Color.Red;
            this.playerNoError.Location = new System.Drawing.Point(431, 145);
            this.playerNoError.Name = "playerNoError";
            this.playerNoError.Size = new System.Drawing.Size(0, 13);
            this.playerNoError.TabIndex = 11;
            // 
            // ErrorMsg
            // 
            this.ErrorMsg.AutoSize = true;
            this.ErrorMsg.Location = new System.Drawing.Point(104, 425);
            this.ErrorMsg.Name = "ErrorMsg";
            this.ErrorMsg.Size = new System.Drawing.Size(35, 13);
            this.ErrorMsg.TabIndex = 12;
            this.ErrorMsg.Text = "label5";
            // 
            // category
            // 
            this.category.FormattingEnabled = true;
            this.category.Items.AddRange(new object[] {
            "all",
            "sports",
            "math",
            "science",
            "films",
            "food"});
            this.category.Location = new System.Drawing.Point(281, 356);
            this.category.Name = "category";
            this.category.Size = new System.Drawing.Size(182, 21);
            this.category.TabIndex = 14;
            this.category.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // CreateRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.ssasafsd;
            this.ClientSize = new System.Drawing.Size(553, 461);
            this.Controls.Add(this.category);
            this.Controls.Add(this.ErrorMsg);
            this.Controls.Add(this.playerNoError);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.back);
            this.Controls.Add(this.Send);
            this.Controls.Add(this.playerNo);
            this.Controls.Add(this.questionTime);
            this.Controls.Add(this.questionsNo);
            this.Controls.Add(this.roomName);
            this.Name = "CreateRoom";
            this.Text = "CreateRoom";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox roomName;
        private System.Windows.Forms.TextBox questionsNo;
        private System.Windows.Forms.TextBox questionTime;
        private System.Windows.Forms.TextBox playerNo;
        private System.Windows.Forms.Button Send;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label userName;
        private System.Windows.Forms.Label playerNoError;
        private System.Windows.Forms.Label ErrorMsg;
        private System.Windows.Forms.ComboBox category;
    }
}