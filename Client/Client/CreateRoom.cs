﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;

namespace Client
{
    public partial class CreateRoom : Form
    {
        private string username;
        private NetworkStream clientStream;
        private bool check;

        public CreateRoom(string username,NetworkStream networkStream)
        {
            InitializeComponent();

            this.username = username;
            this.clientStream = networkStream;

            Thread listening = new Thread(Listening);

            listening.Start();
        }

        private void Listening()
        {

            this.check = true;

            while (check)
            {
                try
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead = clientStream.Read(buffer, 0, 4096);
                    Array.Resize<byte>(ref buffer, bytesRead);
                    string input = new ASCIIEncoding().GetString(buffer);



                    //לטפל בהודעות

                    if (input.Equals("1140"))    //success create room
                    {
                        this.check = false;
                        //open wait game form (in admin mode)
                        ListBox temp = new ListBox();
                        temp.Items.Add(username);

                        Form waitGame = new WaitForGame(this.clientStream, true, temp, int.Parse(questionsNo.Text), int.Parse(questionTime.Text),roomName.Text,int.Parse(playerNo.Text),this.username);

                        Invoke((MethodInvoker)delegate { this.Hide(); });
                        Invoke((MethodInvoker)delegate { waitGame.Show(); });
                        Invoke((MethodInvoker)delegate { this.Close(); });

                    }
                    else if (input.Equals("1141"))  //fail create room
                    {
                        Invoke((MethodInvoker)delegate { ErrorMsg.Text = "Error create room 1141"; });
                    }
                    else    //outer fail
                    {
                        Invoke((MethodInvoker)delegate { ErrorMsg.Text = "Other Error"; });
                    }

                }
                catch
                {

                }
            }

        }

        private void Send_Click(object sender, EventArgs e)
        {
            string message = "213";

            if(roomName.Text.Length < 10)
            {
                message = message + "0" +roomName.Text.Length.ToString() + roomName.Text;
            }
            else
            {
                message = message + roomName.Text.Length.ToString() + roomName.Text;
            }
            

            message = message + playerNo.Text;

            if(int.Parse(questionsNo.Text) < 10)
            {
                message = message + "0" + questionsNo.Text;
            }
            else
            {
                message = message + questionsNo.Text;
            }

            if(int.Parse(questionTime.Text) < 10)
            {
                message = message + "0" + questionTime.Text;
            }
            else
            {
                message = message + questionTime.Text;
            }

            if(category.SelectedItem.ToString().Length < 10)
            {
                message = message + "0" + category.SelectedItem.ToString().Length.ToString();
            }
            else
            {
                message = message + category.SelectedItem.ToString().Length.ToString();
            }

            message = message + category.SelectedItem.ToString();

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }

        private void playerNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (int.Parse(playerNo.Text) > 10)
                {
                    playerNoError.Text = "Error only 1 to 9 players (1-9)";
                }
            }
            catch
            {

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void back_Click(object sender, EventArgs e)
        {
            Form navigate = new navigateForm(this.username, this.clientStream,true);

            this.Hide();

            navigate.Show();

            this.Close();
        }
    }
}
