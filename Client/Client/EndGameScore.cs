﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;

namespace Client
{
    public partial class EndGameScore : Form
    {
        private string[,] users;
        private Form gameForm;
        private string username;
        private NetworkStream clientStream;

        public EndGameScore(string[,] users, Form gameForm,string username,NetworkStream clientStream)
        {
            InitializeComponent();

            this.users = users;
            this.gameForm = gameForm;
            this.username = username;
            this.clientStream = clientStream;

            showResults();
        }

        private void showResults()
        {
            for(int i = 0; i < users.GetLength(0); i++)
            {
                label1.Text = label1.Text + users[i, 0] + " - " + users[i, 1] + "\n";
            }
        }

        private void EndGameScore_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            gameForm.Hide();

            Form navigate = new navigateForm(this.username, this.clientStream);

            navigate.Show();

            this.Close();
            gameForm.Close();
        }
    }
}
