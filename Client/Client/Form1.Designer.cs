﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SignUp = new System.Windows.Forms.Button();
            this.Username = new System.Windows.Forms.TextBox();
            this.Password = new System.Windows.Forms.TextBox();
            this.SignIn = new System.Windows.Forms.Button();
            this.SignInError = new System.Windows.Forms.Label();
            this.ErrorMsg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SignUp
            // 
            this.SignUp.AutoSize = true;
            this.SignUp.BackColor = System.Drawing.Color.Transparent;
            this.SignUp.BackgroundImage = global::Client.Properties.Resources.SignUp;
            this.SignUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SignUp.FlatAppearance.BorderSize = 0;
            this.SignUp.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignUp.ForeColor = System.Drawing.Color.Transparent;
            this.SignUp.Location = new System.Drawing.Point(457, 237);
            this.SignUp.Name = "SignUp";
            this.SignUp.Size = new System.Drawing.Size(89, 27);
            this.SignUp.TabIndex = 0;
            this.SignUp.UseVisualStyleBackColor = false;
            this.SignUp.Click += new System.EventHandler(this.SignUp_Click);
            // 
            // Username
            // 
            this.Username.BackColor = System.Drawing.Color.White;
            this.Username.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Username.Location = new System.Drawing.Point(389, 142);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(207, 19);
            this.Username.TabIndex = 1;
            this.Username.Text = "user";
            // 
            // Password
            // 
            this.Password.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Password.Location = new System.Drawing.Point(389, 196);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(207, 19);
            this.Password.TabIndex = 2;
            this.Password.Text = "Aa12";
            // 
            // SignIn
            // 
            this.SignIn.BackColor = System.Drawing.Color.Transparent;
            this.SignIn.BackgroundImage = global::Client.Properties.Resources.SignIn;
            this.SignIn.FlatAppearance.BorderSize = 0;
            this.SignIn.ForeColor = System.Drawing.Color.Black;
            this.SignIn.Location = new System.Drawing.Point(362, 237);
            this.SignIn.Name = "SignIn";
            this.SignIn.Size = new System.Drawing.Size(89, 27);
            this.SignIn.TabIndex = 3;
            this.SignIn.UseVisualStyleBackColor = false;
            this.SignIn.Click += new System.EventHandler(this.SignIn_Click);
            // 
            // SignInError
            // 
            this.SignInError.AutoSize = true;
            this.SignInError.ForeColor = System.Drawing.Color.Red;
            this.SignInError.Location = new System.Drawing.Point(76, 148);
            this.SignInError.Name = "SignInError";
            this.SignInError.Size = new System.Drawing.Size(0, 13);
            this.SignInError.TabIndex = 6;
            // 
            // ErrorMsg
            // 
            this.ErrorMsg.AutoSize = true;
            this.ErrorMsg.BackColor = System.Drawing.Color.Transparent;
            this.ErrorMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.ErrorMsg.ForeColor = System.Drawing.Color.Red;
            this.ErrorMsg.Location = new System.Drawing.Point(357, 276);
            this.ErrorMsg.Name = "ErrorMsg";
            this.ErrorMsg.Size = new System.Drawing.Size(24, 25);
            this.ErrorMsg.TabIndex = 7;
            this.ErrorMsg.Text = "..";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Client.Properties.Resources.mag1;
            this.ClientSize = new System.Drawing.Size(684, 331);
            this.Controls.Add(this.ErrorMsg);
            this.Controls.Add(this.SignInError);
            this.Controls.Add(this.SignIn);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.Username);
            this.Controls.Add(this.SignUp);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SignUp;
        private System.Windows.Forms.TextBox Username;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Button SignIn;
        private System.Windows.Forms.Label SignInError;
        private System.Windows.Forms.Label ErrorMsg;
    }
}

