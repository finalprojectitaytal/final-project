﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;


namespace Client
{
    public partial class Form1 : Form
    {
        private NetworkStream clientStream;
        private string username;
        private bool check;


        public Form1()
        {
            InitializeComponent();

            Thread listening = new Thread(Listening);

            listening.Start();

            ErrorMsg.Text = "";




        }

        private void Listening()
        {
            bool temp = false;

            do
            {
                try
                {
                    TcpClient client = new TcpClient();

                    IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);

                    client.Connect(serverEndPoint);

                    clientStream = client.GetStream();    // open port
                    temp = true;
                }
                catch
                {

                }
            } while (temp == false);

            this.check = true;

            while (check)
            {
                try
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead = clientStream.Read(buffer, 0, 4096);
                    Array.Resize<byte>(ref buffer, bytesRead);
                    string input = new ASCIIEncoding().GetString(buffer);


                    string msgCode = input.Substring(0, 3);

                    if (msgCode.CompareTo("102") == 0)       //Sign in
                    {
                        Sign_in(input);
                    }
                    else if (msgCode.CompareTo("104") == 0)  //Sign up 
                    {
                        Sign_up(input);
                    }


                    Thread.Sleep(500);
                }
                catch
                {

                }
            }

        }


        private void SendMessage(string message)
        {
            try
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(message);

                clientStream.Write(buffer, 0, message.Length);
                clientStream.Flush();
            }
            catch
            {

            }
        }

        private void SignIn_Click(object sender, EventArgs e)
        {
            string username = Username.Text;
            string password = Password.Text;

            string message = "200";

            if (username.Length > 9)
            {
                message = message + username.Length.ToString() + username;
            }
            else
            {
                message = message + '0' + username.Length.ToString() + username;
            }

            if (password.Length > 9)
            {
                message = message + password.Length.ToString() + password;
            }
            else
            {
                message = message + '0' + password.Length.ToString() + password;
            }

            this.username = username;
            Thread send = new Thread(() => SendMessage(message));
            send.Start();
        }

        private void Sign_in(string message)
        {
            if (message.CompareTo("1020") == 0)      //success
            {
                this.check = false;
                //open form 2
                Form navigateForm = new navigateForm(this.username, this.clientStream);

                Invoke((MethodInvoker)delegate { this.Hide(); });
                Invoke((MethodInvoker)delegate { navigateForm.Show(); });



            }
            else if (message.CompareTo("1021") == 0) //Wrong Details
            {
                Invoke((MethodInvoker)delegate { ErrorMsg.Text = "Worng Details"; });
            }
            else if (message.CompareTo("1022") == 0) //User is already connected
            {
                Invoke((MethodInvoker)delegate { ErrorMsg.Text = "User is already connected"; });

            }

        }

        private void Sign_up(string message)
        {
            if (message.CompareTo("1040") == 0)
            {

                Form signup = new Signup(this, this.clientStream);

                Invoke((MethodInvoker)delegate { this.Hide(); });

                Invoke((MethodInvoker)delegate { signup.Show(); });
            }
            else if (message.CompareTo("1041") == 0)
            {
                Form singup = new Signup(this, this.clientStream, "Password illegal");

                Invoke((MethodInvoker)delegate { this.Hide(); });

                Invoke((MethodInvoker)delegate { singup.Show(); });
            }
            else if (message.CompareTo("1042") == 0)
            {
                Form singup = new Signup(this, this.clientStream, "Username is already exists");

                Invoke((MethodInvoker)delegate { this.Hide(); });

                Invoke((MethodInvoker)delegate { singup.Show(); });
            }
            else if (message.CompareTo("1043") == 0)
            {
                Form singup = new Signup(this, this.clientStream, "Username is illegal");

                Invoke((MethodInvoker)delegate { this.Hide(); });

                Invoke((MethodInvoker)delegate { singup.Show(); });
            }
            else if (message.CompareTo("1044") == 0)
            {
                Form singup = new Signup(this, this.clientStream, "Other");

                Invoke((MethodInvoker)delegate { this.Hide(); });

                Invoke((MethodInvoker)delegate { singup.Show(); });
            }
        }

        private void SignUp_Click(object sender, EventArgs e)
        {
            Form singup = new Signup(this, this.clientStream);

            Invoke((MethodInvoker)delegate { this.Hide(); });

            Invoke((MethodInvoker)delegate { singup.Show(); });
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
