﻿namespace Client
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Question = new System.Windows.Forms.Label();
            this.Answer1 = new System.Windows.Forms.Button();
            this.Answer2 = new System.Windows.Forms.Button();
            this.Answer3 = new System.Windows.Forms.Button();
            this.Answer4 = new System.Windows.Forms.Button();
            this.score = new System.Windows.Forms.Label();
            this.timerLable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Question
            // 
            this.Question.AutoSize = true;
            this.Question.Location = new System.Drawing.Point(96, 151);
            this.Question.MaximumSize = new System.Drawing.Size(350, 0);
            this.Question.Name = "Question";
            this.Question.Size = new System.Drawing.Size(47, 13);
            this.Question.TabIndex = 0;
            this.Question.Text = "question";
            // 
            // Answer1
            // 
            this.Answer1.BackColor = System.Drawing.Color.Gainsboro;
            this.Answer1.Location = new System.Drawing.Point(90, 250);
            this.Answer1.Name = "Answer1";
            this.Answer1.Size = new System.Drawing.Size(371, 33);
            this.Answer1.TabIndex = 1;
            this.Answer1.Text = "button1";
            this.Answer1.UseVisualStyleBackColor = false;
            this.Answer1.Click += new System.EventHandler(this.answer1_Click);
            // 
            // Answer2
            // 
            this.Answer2.Location = new System.Drawing.Point(90, 303);
            this.Answer2.Name = "Answer2";
            this.Answer2.Size = new System.Drawing.Size(371, 33);
            this.Answer2.TabIndex = 2;
            this.Answer2.Text = "button2";
            this.Answer2.UseVisualStyleBackColor = true;
            this.Answer2.Click += new System.EventHandler(this.answer2_Click);
            // 
            // Answer3
            // 
            this.Answer3.Location = new System.Drawing.Point(90, 355);
            this.Answer3.Name = "Answer3";
            this.Answer3.Size = new System.Drawing.Size(371, 33);
            this.Answer3.TabIndex = 3;
            this.Answer3.Text = "button3";
            this.Answer3.UseVisualStyleBackColor = true;
            this.Answer3.Click += new System.EventHandler(this.answer3_Click);
            // 
            // Answer4
            // 
            this.Answer4.Location = new System.Drawing.Point(90, 408);
            this.Answer4.Name = "Answer4";
            this.Answer4.Size = new System.Drawing.Size(371, 33);
            this.Answer4.TabIndex = 4;
            this.Answer4.Text = "button4";
            this.Answer4.UseVisualStyleBackColor = true;
            this.Answer4.Click += new System.EventHandler(this.answer4_Click);
            // 
            // score
            // 
            this.score.AutoSize = true;
            this.score.BackColor = System.Drawing.Color.Transparent;
            this.score.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.score.Location = new System.Drawing.Point(27, 39);
            this.score.Name = "score";
            this.score.Size = new System.Drawing.Size(54, 25);
            this.score.TabIndex = 5;
            this.score.Text = "0 / 0";
            // 
            // timerLable
            // 
            this.timerLable.AutoSize = true;
            this.timerLable.BackColor = System.Drawing.Color.Transparent;
            this.timerLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.timerLable.Location = new System.Drawing.Point(110, 40);
            this.timerLable.Name = "timerLable";
            this.timerLable.Size = new System.Drawing.Size(60, 24);
            this.timerLable.TabIndex = 6;
            this.timerLable.Text = "label1";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.Untitled_asdasdasdasd2;
            this.ClientSize = new System.Drawing.Size(534, 501);
            this.Controls.Add(this.timerLable);
            this.Controls.Add(this.score);
            this.Controls.Add(this.Answer4);
            this.Controls.Add(this.Answer3);
            this.Controls.Add(this.Answer2);
            this.Controls.Add(this.Answer1);
            this.Controls.Add(this.Question);
            this.Name = "Game";
            this.Text = "Game";
            this.Load += new System.EventHandler(this.Game_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Question;
        private System.Windows.Forms.Button Answer1;
        private System.Windows.Forms.Button Answer2;
        private System.Windows.Forms.Button Answer3;
        private System.Windows.Forms.Button Answer4;
        private System.Windows.Forms.Label score;
        private System.Windows.Forms.Label timerLable;
    }
}