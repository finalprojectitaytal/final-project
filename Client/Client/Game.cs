﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;


namespace Client
{
    public partial class Game : Form
    {
        private bool check;
        private string username;
        private NetworkStream clientStream;


        private int questionNumber;
        private int questiontime;

        private int timeLeft;
        private int lastAnswer;

        private int correctAnswer;
        private int answersNo;

        private bool temp;

        private System.Windows.Forms.Timer timer1;


        public Game(int questionNumber, int questiontime, NetworkStream clientStream,string firstQuestion,string username)
        {
            InitializeComponent();

            timerLable.Text = questiontime.ToString();
            this.questionNumber = questionNumber;
            this.questiontime = questiontime;
            this.username = username;
            this.clientStream = clientStream;

            this.timeLeft = questiontime;

            this.temp = false;

            this.lastAnswer = 5;

            this.correctAnswer = 0;
            this.answersNo = 0;

            nextQuestion(firstQuestion);


            Thread listening = new Thread(Listening);
            listening.Start();
        }

        private void Listening()
        {

            this.check = true;

            while (check)
            {
                try
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead = clientStream.Read(buffer, 0, 4096);
                    Array.Resize<byte>(ref buffer, bytesRead);
                    string input = new ASCIIEncoding().GetString(buffer);


                    string msgCode = input.Substring(0, 3);

                    //לטפל בהודעות
                    if(msgCode.Equals("118"))   //next question
                    {
                        Thread.Sleep(500);
                        Invoke((MethodInvoker)delegate { Answer1.BackColor = Color.Gainsboro; });
                        Invoke((MethodInvoker)delegate { Answer2.BackColor = Color.Gainsboro; });
                        Invoke((MethodInvoker)delegate { Answer3.BackColor = Color.Gainsboro; });
                        Invoke((MethodInvoker)delegate { Answer4.BackColor = Color.Gainsboro; });

                        this.lastAnswer = 5;
                        this.answersNo++;
                        nextQuestion(input);
                    }
                    else if(msgCode.Equals("120"))   //true or false answer
                    {
                        lastAnswerIndication(input);
                    }
                    else if(msgCode.Equals("121"))  //end game
                    {
                        this.check = false;
                        end_game(input);
                    }


                }
                catch
                {

                }
            }

        }

        private void end_game(string message)
        {
            int index = 4;

            int usersNo = int.Parse(message[3].ToString());

            string[,] users = new string[usersNo, 2];

            for(int i = 0; i < usersNo; i++)
            {
                int userLen = int.Parse(message.Substring(index, 2));
                index = index + 2;

                string username = message.Substring(index, userLen);
                index = index + userLen;

                string score = message.Substring(index, 2);
                index = index + 2;

                users[i, 0] = username;
                users[i, 1] = score;
            }

            //open score form
            Form end = new EndGameScore(users, this,this.username,this.clientStream);
            Invoke((MethodInvoker)delegate { end.Show(); });

        }

        private void nextQuestion(string message)
        {
            this.temp = false;
            this.timeLeft = this.questiontime;
            try
            {
                timerLable.Text = this.timeLeft.ToString();
            }
            catch
            {
                Invoke((MethodInvoker)delegate{ timerLable.Text = this.timeLeft.ToString(); });
            }

            try
            {
                try
                {
                    Game_Load(null, null);
                }
                catch
                {
                    Invoke((MethodInvoker)delegate { Game_Load(null, null); });
                        
                }
            }
            catch
            {

            }
            int index = 3;

            int questionLen = int.Parse(message.Substring(index, 3));
            index = index + 3;

            string question = message.Substring(index, questionLen);
            index = index + questionLen;

            int answer1Len = int.Parse(message.Substring(index, 3));
            index = index + 3;

            string answer1 = message.Substring(index, answer1Len);
            index = index + answer1Len;

            int answer2Len = int.Parse(message.Substring(index, 3));
            index = index + 3;

            string answer2 = message.Substring(index, answer2Len);
            index = index + answer2Len;

            int answer3Len = int.Parse(message.Substring(index, 3));
            index = index + 3;

            string answer3 = message.Substring(index, answer3Len);
            index = index + answer3Len;

            int answer4Len = int.Parse(message.Substring(index, 3));
            index = index + 3;

            string answer4 = message.Substring(index, answer4Len);
            index = index + answer4Len;

            try
            {
                Invoke((MethodInvoker)delegate { Question.Text = question; });
                Invoke((MethodInvoker)delegate { Answer1.Text = answer1; });
                Invoke((MethodInvoker)delegate { Answer2.Text = answer2; });
                Invoke((MethodInvoker)delegate { Answer3.Text = answer3; });
                Invoke((MethodInvoker)delegate { Answer4.Text = answer4; });

                Invoke((MethodInvoker)delegate { score.Text = this.correctAnswer.ToString() + " / " + this.answersNo.ToString(); });
            }
            catch
            {
                Question.Text = question;
                Answer1.Text = answer1;
                Answer2.Text = answer2;
                Answer3.Text = answer3;
                Answer4.Text = answer4;

                score.Text = this.correctAnswer.ToString() + " / " + this.answersNo.ToString();
            }

            
        }

        private void lastAnswerIndication(string message)
        {
            int indication = int.Parse(message[3].ToString());

            if(this.lastAnswer == 1)
            {
                if(indication == 1)
                {
                    Invoke((MethodInvoker)delegate { Answer1.BackColor = Color.Green; });
                    this.correctAnswer++;
                }
                else
                {
                    Invoke((MethodInvoker)delegate { Answer1.BackColor = Color.Red; });
                }
            }
            else if(this.lastAnswer == 2)
            {
                if (indication == 1)
                {
                    Invoke((MethodInvoker)delegate { Answer2.BackColor = Color.Green; });
                    this.correctAnswer++;
                }
                else
                {
                    Invoke((MethodInvoker)delegate { Answer2.BackColor = Color.Red; });
                }
            }
            else if(this.lastAnswer == 3)
            {
                if (indication == 1)
                {
                    Invoke((MethodInvoker)delegate { Answer3.BackColor = Color.Green; });
                    this.correctAnswer++;
                }
                else
                {
                    Invoke((MethodInvoker)delegate { Answer3.BackColor = Color.Red; });
                }
            }
            else if(this.lastAnswer == 4)
            {
                if (indication == 1)
                {
                    Invoke((MethodInvoker)delegate { Answer4.BackColor = Color.Green; });
                    this.correctAnswer++;
                }
                else
                {
                    Invoke((MethodInvoker)delegate { Answer4.BackColor = Color.Red; });
                }
            }
            else if(this.lastAnswer == 5)
            {
                Invoke((MethodInvoker)delegate { Answer1.BackColor = Color.Red; });
                Invoke((MethodInvoker)delegate { Answer2.BackColor = Color.Red; });
                Invoke((MethodInvoker)delegate { Answer3.BackColor = Color.Red; });
                Invoke((MethodInvoker)delegate { Answer4.BackColor = Color.Red; });
            }
        }



        private void answer1_Click(object sender, EventArgs e)
        {
            this.temp = true;

            this.lastAnswer = 1;

            string message = "2191" + (this.questiontime - this.timeLeft).ToString();
            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();

            this.timer1.Stop();
            this.timeLeft = this.questiontime;
        }

        private void answer2_Click(object sender, EventArgs e)
        {
            this.temp = true;

            this.lastAnswer = 2;

            string message = "2192" + (this.questiontime - this.timeLeft).ToString();
            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();


            this.timeLeft = this.questiontime + 1;
        }

        private void answer3_Click(object sender, EventArgs e)
        {
            this.temp = true;

            this.lastAnswer = 3;

            string message = "2193" + (this.questiontime - this.timeLeft).ToString();
            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();

            this.timeLeft = this.questiontime + 1;
        }

        private void answer4_Click(object sender, EventArgs e)
        {
            this.temp = true;

            this.lastAnswer = 4;

            string message = "2194" + (this.questiontime - this.timeLeft).ToString();
            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();

            this.timeLeft = this.questiontime + 1;
        }

        private void Game_Load(object sender, EventArgs e)
        {
            this.timer1 = new System.Windows.Forms.Timer();

            timer1.Interval = (1 * 1000); // 1 sec
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.timeLeft--;
            timerLable.Text = this.timeLeft.ToString();


            if(this.timeLeft == 0 && this.temp == false)
            {
                string message = "2195" + this.questiontime;
                byte[] buffer = new ASCIIEncoding().GetBytes(message);

                clientStream.Write(buffer, 0, message.Length);
                clientStream.Flush();

                this.timeLeft = this.questiontime + 1;
            }

        }

        private void Game_FormClosing(object sender, FormClosingEventArgs e)
        {
            string message = "201";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }
    }
}
