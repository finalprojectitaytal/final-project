﻿namespace Client
{
    partial class MyStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numOfGames = new System.Windows.Forms.Label();
            this.correctAns = new System.Windows.Forms.Label();
            this.wrongAns = new System.Windows.Forms.Label();
            this.avgTime = new System.Windows.Forms.Label();
            this.back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // numOfGames
            // 
            this.numOfGames.AutoSize = true;
            this.numOfGames.BackColor = System.Drawing.Color.Transparent;
            this.numOfGames.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.numOfGames.ForeColor = System.Drawing.Color.White;
            this.numOfGames.Location = new System.Drawing.Point(306, 155);
            this.numOfGames.Name = "numOfGames";
            this.numOfGames.Size = new System.Drawing.Size(140, 18);
            this.numOfGames.TabIndex = 0;
            this.numOfGames.Text = "Number of games - ";
            // 
            // correctAns
            // 
            this.correctAns.AutoSize = true;
            this.correctAns.BackColor = System.Drawing.Color.Transparent;
            this.correctAns.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.correctAns.ForeColor = System.Drawing.Color.White;
            this.correctAns.Location = new System.Drawing.Point(306, 190);
            this.correctAns.Name = "correctAns";
            this.correctAns.Size = new System.Drawing.Size(131, 18);
            this.correctAns.TabIndex = 1;
            this.correctAns.Text = "Correct answers - ";
            // 
            // wrongAns
            // 
            this.wrongAns.AutoSize = true;
            this.wrongAns.BackColor = System.Drawing.Color.Transparent;
            this.wrongAns.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.wrongAns.ForeColor = System.Drawing.Color.White;
            this.wrongAns.Location = new System.Drawing.Point(292, 231);
            this.wrongAns.Name = "wrongAns";
            this.wrongAns.Size = new System.Drawing.Size(126, 18);
            this.wrongAns.TabIndex = 2;
            this.wrongAns.Text = "Wrong answers - ";
            // 
            // avgTime
            // 
            this.avgTime.AutoSize = true;
            this.avgTime.BackColor = System.Drawing.Color.Transparent;
            this.avgTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.avgTime.ForeColor = System.Drawing.Color.White;
            this.avgTime.Location = new System.Drawing.Point(362, 269);
            this.avgTime.Name = "avgTime";
            this.avgTime.Size = new System.Drawing.Size(162, 18);
            this.avgTime.TabIndex = 3;
            this.avgTime.Text = "Average time to answer";
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(86, 28);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(67, 31);
            this.back.TabIndex = 4;
            this.back.Text = "back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // MyStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.status;
            this.ClientSize = new System.Drawing.Size(524, 461);
            this.Controls.Add(this.back);
            this.Controls.Add(this.avgTime);
            this.Controls.Add(this.wrongAns);
            this.Controls.Add(this.correctAns);
            this.Controls.Add(this.numOfGames);
            this.Name = "MyStatus";
            this.Text = "MyStatus";
            this.Load += new System.EventHandler(this.MyStatus_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label numOfGames;
        private System.Windows.Forms.Label correctAns;
        private System.Windows.Forms.Label wrongAns;
        private System.Windows.Forms.Label avgTime;
        private System.Windows.Forms.Button back;
    }
}