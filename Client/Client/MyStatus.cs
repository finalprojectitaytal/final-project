﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;

namespace Client
{
    public partial class MyStatus : Form
    {
        private string data;
        private Form navigate;
        private NetworkStream clientStream;

        public MyStatus(Form navigate, string data,NetworkStream clientStream)
        {
            InitializeComponent();
            this.navigate = navigate;
            this.data = data;
            this.clientStream = clientStream;

            GetData();
        }

        public void GetData()
        {
            string dataWithoutCode = data.Substring(3);
            int index = -1;
            int i = 0;

            if(!dataWithoutCode.Equals("0000"))
            {
                string numberOfGames = dataWithoutCode.Substring(0, 4);

                int len = numberOfGames.Length;

                while(i < len)
                {
                    if(numberOfGames[i] == '0')
                    {
                        index = i;
                    }
                    else
                    {
                        i = len;
                    }

                    i++;
                }

                if(index != -1)
                {
                    numberOfGames = numberOfGames.Substring(index + 1);
                }

                index = -1;
                i = 0;
                
                string correctAnswers = dataWithoutCode.Substring(4, 6);
                len = correctAnswers.Length;

                while (i < len)
                {
                    if (correctAnswers[i] == '0')
                    {
                        index = i;
                    }
                    else
                    {
                        i = len;
                    }

                    i++;
                }

                if (index != -1)
                {
                    correctAnswers = correctAnswers.Substring(index + 1);
                }

                index = -1;
                i = 0;

                string wrongAnswers = dataWithoutCode.Substring(10, 6);
                len = wrongAnswers.Length;
                while (i < len)
                {
                    if (wrongAnswers[i] == '0')
                    {
                        index = i;
                    }
                    else
                    {
                        i = len;
                    }

                    i++;
                }

                if (index != -1)
                {
                    wrongAnswers = wrongAnswers.Substring(index + 1);
                }

                index = -1;
                i = 0;

                string avgTimeToAns = dataWithoutCode.Substring(16, 4);
                string first = avgTimeToAns.Substring(0, 2);
                string last = avgTimeToAns.Substring(2);
                len = first.Length;
                while (i < len)
                {
                    if (first[i] == '0')
                    {
                        index = i;
                    }
                    else
                    {
                        i = len;
                    }

                    i++;
                }
                if (index != -1)
                {
                    first = first.Substring(index + 1);
                }

                numOfGames.Text =numberOfGames;
                correctAns.Text =correctAnswers;
                wrongAns.Text =wrongAnswers;
                avgTime.Text =first + "." + last;
            }
            else
            {
                numOfGames.Text ="0";
                correctAns.Text = "0";
                wrongAns.Text =  "0";
                avgTime.Text =  "0";
            }


        }

        private void MyStatus_Load(object sender, EventArgs e)
        {

        }

        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            navigate.Show();
            this.Close();
        }

        private void MyStatus_FormClosed(object sender, FormClosedEventArgs e)
        {
            string message = "201";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }
    }
}
