﻿namespace Client
{
    partial class RoomMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.room_list = new System.Windows.Forms.ListBox();
            this.ErrorMsg = new System.Windows.Forms.Label();
            this.Join_Room = new System.Windows.Forms.Button();
            this.refresh = new System.Windows.Forms.Button();
            this.User_list = new System.Windows.Forms.ListBox();
            this.back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // room_list
            // 
            this.room_list.BackColor = System.Drawing.Color.White;
            this.room_list.FormattingEnabled = true;
            this.room_list.Location = new System.Drawing.Point(112, 116);
            this.room_list.Name = "room_list";
            this.room_list.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.room_list.Size = new System.Drawing.Size(341, 95);
            this.room_list.TabIndex = 0;
            this.room_list.SelectedIndexChanged += new System.EventHandler(this.room_list_SelectedIndexChanged);
            // 
            // ErrorMsg
            // 
            this.ErrorMsg.AutoSize = true;
            this.ErrorMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.ErrorMsg.ForeColor = System.Drawing.Color.Red;
            this.ErrorMsg.Location = new System.Drawing.Point(346, 470);
            this.ErrorMsg.Name = "ErrorMsg";
            this.ErrorMsg.Size = new System.Drawing.Size(16, 18);
            this.ErrorMsg.TabIndex = 1;
            this.ErrorMsg.Text = "..";
            // 
            // Join_Room
            // 
            this.Join_Room.Location = new System.Drawing.Point(231, 412);
            this.Join_Room.Name = "Join_Room";
            this.Join_Room.Size = new System.Drawing.Size(97, 20);
            this.Join_Room.TabIndex = 2;
            this.Join_Room.Text = "Join Room";
            this.Join_Room.UseVisualStyleBackColor = true;
            this.Join_Room.Click += new System.EventHandler(this.Join_Room_Click);
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(231, 457);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(97, 20);
            this.refresh.TabIndex = 3;
            this.refresh.Text = "Refresh";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // User_list
            // 
            this.User_list.FormattingEnabled = true;
            this.User_list.Location = new System.Drawing.Point(112, 300);
            this.User_list.Name = "User_list";
            this.User_list.Size = new System.Drawing.Size(340, 82);
            this.User_list.TabIndex = 4;
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(473, 26);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(49, 19);
            this.back.TabIndex = 5;
            this.back.Text = "back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // RoomMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.Untitleasdasdasdd_1;
            this.ClientSize = new System.Drawing.Size(534, 511);
            this.Controls.Add(this.back);
            this.Controls.Add(this.User_list);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.Join_Room);
            this.Controls.Add(this.ErrorMsg);
            this.Controls.Add(this.room_list);
            this.Name = "RoomMenu";
            this.Text = "RoomMenu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox room_list;
        private System.Windows.Forms.Label ErrorMsg;
        private System.Windows.Forms.Button Join_Room;
        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.ListBox User_list;
        private System.Windows.Forms.Button back;
    }
}