﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;

namespace Client
{
    public partial class RoomMenu : Form
    {
        private string[,] roomList;
        private string username;
        private NetworkStream clientStream;
        private bool check;
        private string currRoom;
        private string id;

        public RoomMenu(string[,] roomList, NetworkStream clientStream,string username)
        {
            InitializeComponent();
            this.roomList = roomList;
            this.username = username;
            this.clientStream = clientStream;

            this.currRoom = "";

            User_list.Hide();

            Thread listening = new Thread(Listening);

            listening.Start();

            createRoomList();
        }

        private void createRoomList()
        {
            for(int i = 0;i<this.roomList.GetLength(0);i++)
            {
                room_list.Items.Add(this.roomList[i, 1]);
            }
        }

        private void Listening()
        {

            this.check = true;

            while (check)
            {
                try
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead = clientStream.Read(buffer, 0, 4096);
                    Array.Resize<byte>(ref buffer, bytesRead);
                    string input = new ASCIIEncoding().GetString(buffer);


                    string msgCode = input.Substring(0, 3);

                    //לטפל בהודעות
                    if(msgCode.Equals("106"))   //get rooms
                    {
                        getRooms(input);
                    }
                    else if(msgCode.Equals("108")) //get users in room.
                    {
                        init_user_list(input);
                    }
                    else if(msgCode.Equals("110"))
                    {
                        this.check = false;
                        joinRoom(input);
                    }

                }
                catch
                {

                }
            }

        }

        private void joinRoom(string message)
        {
            int index = 3;


            int status = int.Parse(message.Substring(index, 1));
            index++;

            if(status == 0)
            {
                this.check = false;

                int questionNumber = int.Parse(message.Substring(index, 2));
                index = index + 2;

                int questiontime = int.Parse(message.Substring(index, 2));
                index = index + 2;

                //call to form of wait for game (not in admin mode)
                Form gameWait = new WaitForGame(this.clientStream, false, User_list, questionNumber, questiontime, this.currRoom,this.username,this.id);
                
                Invoke((MethodInvoker)delegate { this.Hide(); });
                Invoke((MethodInvoker)delegate { gameWait.Show(); });
                Invoke((MethodInvoker)delegate { this.Close(); });

            }
            else if(status == 1)
            {
                this.check = true;
                ErrorMsg.Text = "failed - room is full";
            }
            else if(status == 2)
            {
                this.check = true;
                ErrorMsg.Text = "failed - room not exist or other reason";
            }
        }

        private void init_user_list(string message)
        {
            int index = 3;
            Invoke((MethodInvoker)delegate { User_list.Items.Clear(); });
            int playersNo = int.Parse(message.Substring(index, 1));
            index++;

            for(int i = 0; i<playersNo; i++)
            {
                int usernameLen = int.Parse(message.Substring(index, 2));
                index = index + 2;

                string username = message.Substring(index, usernameLen);
                index = index + usernameLen;

                Invoke((MethodInvoker)delegate { User_list.Items.Add(username); });
            }
        }

        private void room_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            User_list.Show();

            //user list chenge.

            this.currRoom = room_list.SelectedIndex.ToString();

            


            string message = "207";
            if (int.Parse(currRoom) != -1)
            {
                message = message + this.roomList[int.Parse(currRoom), 0];


                /*for (int i = 0;i<this.roomList.GetLength(0);i++)
                {
                    if(this.roomList[i,1].Equals(this.currRoom))
                    {
                        message = message + this.roomList[i, 0];
                    }
                }*/

                byte[] buffer = new ASCIIEncoding().GetBytes(message);

                clientStream.Write(buffer, 0, message.Length);
                clientStream.Flush();
            }

        }

        private void Join_Room_Click(object sender, EventArgs e)
        {
            User_list.Show();
            string roomID = "0000";

            if (this.currRoom.CompareTo("") != 0)
            {
                roomID = this.roomList[int.Parse(this.currRoom), 0];
                string message = "209" + roomID;

                this.id = roomID;

                byte[] buffer = new ASCIIEncoding().GetBytes(message);

                clientStream.Write(buffer, 0, message.Length);
                clientStream.Flush();
            }

            
        }

        private void getRooms(string message)
        { 
            Invoke((MethodInvoker)delegate { room_list.Items.Clear(); });

            string[,] rooms;

            int index = 3;

            int roomNum = int.Parse(message.Substring(index, 4));

            index = index + 4;

            rooms = new string[roomNum, 2];

            for (int i = 0; i < roomNum; i++)
            {
                string roomID = message.Substring(index, 4);
                index = index + 4;

                int roomLen = int.Parse(message.Substring(index, 2));
                index = index + 2;

                string roomName = message.Substring(index, roomLen);
                index = index + roomLen;

                rooms[i, 0] = roomID;
                rooms[i, 1] = roomName;


            }
            this.roomList = rooms;

            for (int i = 0; i < this.roomList.GetLength(0); i++)
            {
                Invoke((MethodInvoker)delegate { room_list.Items.Add(this.roomList[i, 1]); });
            }
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            string message = "205";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();

        }

        private void back_Click(object sender, EventArgs e)
        {
            this.check = false;
            this.Hide();

            Form navigate = new navigateForm(this.username, this.clientStream,true);
            navigate.Show();

            this.Close();
        }


    }
}
