﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;

namespace Client
{
    public partial class Signup : Form
    {
        private Form form1;
        private NetworkStream clientStream;

        public Signup(Form form1, NetworkStream clientStream)
        {
            InitializeComponent();
            this.form1 = form1;
            this.clientStream = clientStream;
            ErrorMsg.Text = "";
        }

        public Signup(Form form1, NetworkStream clientStream, string errorMsg)
        {
            InitializeComponent();
            this.form1 = form1;
            this.clientStream = clientStream;

            ErrorMsg.Text = errorMsg;
        }

        private void Signup_ok_Click(object sender, EventArgs e)
        {
            string username = Username.Text;
            string password = Password.Text;
            string email = Email.Text;

            string message = "203";

            if (username.Length > 9)
            {
                message = message + username.Length.ToString() + username;
            }
            else
            {
                message = message + '0' + username.Length.ToString() + username;
            }

            if (password.Length > 9)
            {
                message = message + password.Length.ToString() + password;
            }
            else
            {
                message = message + '0' + password.Length.ToString() + password;
            }

            if(email.Length > 9)
            {
                message = message + email.Length.ToString() + email;
            }
            else
            {
                message = message + '0' + email.Length.ToString() + email;
            }

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();

            this.Hide();

            form1.Show();

            this.Close();
        }

        private void Signup_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();

            this.form1.Show();

            this.Close();
        }
    }
}
