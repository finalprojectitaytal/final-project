﻿namespace Client
{
    partial class WaitForGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.start_game = new System.Windows.Forms.Button();
            this.leave_game = new System.Windows.Forms.Button();
            this.Users = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // start_game
            // 
            this.start_game.Location = new System.Drawing.Point(239, 345);
            this.start_game.Name = "start_game";
            this.start_game.Size = new System.Drawing.Size(85, 33);
            this.start_game.TabIndex = 1;
            this.start_game.Text = "Start Game";
            this.start_game.UseVisualStyleBackColor = true;
            this.start_game.Click += new System.EventHandler(this.start_game_Click);
            // 
            // leave_game
            // 
            this.leave_game.Location = new System.Drawing.Point(239, 384);
            this.leave_game.Name = "leave_game";
            this.leave_game.Size = new System.Drawing.Size(85, 33);
            this.leave_game.TabIndex = 2;
            this.leave_game.Text = "Leave Game";
            this.leave_game.UseVisualStyleBackColor = true;
            this.leave_game.Click += new System.EventHandler(this.leave_game_Click);
            // 
            // Users
            // 
            this.Users.FormattingEnabled = true;
            this.Users.Location = new System.Drawing.Point(96, 116);
            this.Users.Name = "Users";
            this.Users.Size = new System.Drawing.Size(381, 173);
            this.Users.TabIndex = 3;
            // 
            // WaitForGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.Untitledasdd_1;
            this.ClientSize = new System.Drawing.Size(534, 461);
            this.Controls.Add(this.Users);
            this.Controls.Add(this.leave_game);
            this.Controls.Add(this.start_game);
            this.Name = "WaitForGame";
            this.Text = "WaitForGame";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button start_game;
        private System.Windows.Forms.Button leave_game;
        private System.Windows.Forms.ListBox Users;
    }
}