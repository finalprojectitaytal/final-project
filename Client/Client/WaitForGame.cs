﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;
using System.Web;

namespace Client
{
    

    public partial class WaitForGame : Form
    {
        private bool check;
        private bool admin;
        private string username;
        private NetworkStream clientStream;
        private int questionsNumber;
        private int questionTimeInSec;
        private string roomName;
        private int maxPlayers;

        public WaitForGame(NetworkStream clientStream,bool admin, ListBox users,int questionsNumber,int questionTimeInSec,string roomName,string username,string id)
        {
            InitializeComponent();

            this.username = username;

            this.clientStream = clientStream;

            this.admin = admin;

            this.questionsNumber = questionsNumber;

            this.questionTimeInSec = questionTimeInSec;

            this.roomName = roomName;

            //Users.Items.Add(users.Items);

            string message = "209" + id;


            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();

            if (!admin)
            {
                start_game.Hide();
            }

            Thread listening = new Thread(Listening);

            listening.Start();
        }

        public WaitForGame(NetworkStream clientStream, bool admin, ListBox users, int questionsNumber, int questionTimeInSec,string roomName,int maxPlayers,string username)
        {
            InitializeComponent();

            this.username = username;

            this.clientStream = clientStream;

            this.admin = admin;

            this.questionsNumber = questionsNumber;

            this.questionTimeInSec = questionTimeInSec;

            this.roomName = roomName;

            this.maxPlayers = maxPlayers;

            Users.Items.Add(username);

            if (!admin)
            {
                start_game.Hide();
            }

            Thread listening = new Thread(Listening);

            listening.Start();
        }

        private void Listening()
        {

            this.check = true;

            while (check)
            {
                try
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead = clientStream.Read(buffer, 0, 4096);
                    Array.Resize<byte>(ref buffer, bytesRead);
                    string input = new ASCIIEncoding().GetString(buffer);


                    string msgCode = input.Substring(0, 3);


                    //לטפל בהודעות
                    if (msgCode.Equals("112"))  //leave room
                    {
                        this.check = false;

                        Invoke((MethodInvoker)delegate { this.Hide(); });

                        Form navigate = new navigateForm(this.username, this.clientStream,true);

                        Invoke((MethodInvoker)delegate { navigate.Show(); });

                        Invoke((MethodInvoker)delegate { this.Close(); });

                    }
                    else if (msgCode.Equals("118"))     //start game
                    {
                        StartGame(input);
                    }
                    else if(msgCode.Equals("108"))  //update users
                    {
                        updateUserList(input);
                    }
                    else if(msgCode.Equals("116"))
                    {
                        this.check = false;

                        Invoke((MethodInvoker)delegate { this.Hide(); });

                        Form navigate = new navigateForm(this.username, this.clientStream,true);

                        Invoke((MethodInvoker)delegate { navigate.Show(); });

                        Invoke((MethodInvoker)delegate { this.Close(); });
                    }

                }
                catch
                {

                }
            }

        }

        private void StartGame(string message)
        {
            //מעביר לטופס של משחק
            this.check = false;

            Form game = new Game(this.questionsNumber, this.questionTimeInSec, this.clientStream,message,this.username);

            Invoke((MethodInvoker)delegate { this.Hide(); });
            Invoke((MethodInvoker)delegate { game.Show(); });
            Invoke((MethodInvoker)delegate { this.Close(); });

        }

        private void leave_game_Click(object sender, EventArgs e)
        {
            string message;

            if(admin)
            {
                message = "215";
            }
            else
            {
                message = "211";
            }

           

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }

        private void start_game_Click(object sender, EventArgs e)
        {
            string message = "217";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }

        private void updateUserList(string message)
        {
            int index = 3;
            Invoke((MethodInvoker)delegate { Users.Items.Clear(); });

            int playersNo = int.Parse(message.Substring(index, 1));
            index++;

            for (int i = 0; i < playersNo; i++)
            {
                int usernameLen = int.Parse(message.Substring(index, 2));
                index = index + 2;

                string temp = message.Substring(index, usernameLen);
                index = index + usernameLen;

                Invoke((MethodInvoker)delegate { Users.Items.Add(temp); });
            }
        }

        private void WaitForGame_FormClosing(object sender, FormClosingEventArgs e)
        {
            string message = "201";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }
    }
}
