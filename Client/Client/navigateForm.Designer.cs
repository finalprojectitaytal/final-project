﻿namespace Client
{
    partial class navigateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HelloUser = new System.Windows.Forms.Label();
            this.JoinRoom = new System.Windows.Forms.Button();
            this.CreateRoom = new System.Windows.Forms.Button();
            this.MyStatus = new System.Windows.Forms.Button();
            this.BestScore = new System.Windows.Forms.Button();
            this.SignOut = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HelloUser
            // 
            this.HelloUser.AutoSize = true;
            this.HelloUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.HelloUser.Location = new System.Drawing.Point(172, 24);
            this.HelloUser.Name = "HelloUser";
            this.HelloUser.Size = new System.Drawing.Size(60, 24);
            this.HelloUser.TabIndex = 0;
            this.HelloUser.Text = "label1";
            // 
            // JoinRoom
            // 
            this.JoinRoom.BackColor = System.Drawing.Color.Transparent;
            this.JoinRoom.BackgroundImage = global::Client.Properties.Resources.join_room;
            this.JoinRoom.Location = new System.Drawing.Point(186, 161);
            this.JoinRoom.Name = "JoinRoom";
            this.JoinRoom.Size = new System.Drawing.Size(219, 41);
            this.JoinRoom.TabIndex = 1;
            this.JoinRoom.UseVisualStyleBackColor = false;
            this.JoinRoom.Click += new System.EventHandler(this.JoinRoom_Click);
            // 
            // CreateRoom
            // 
            this.CreateRoom.BackColor = System.Drawing.Color.Transparent;
            this.CreateRoom.BackgroundImage = global::Client.Properties.Resources.create_room;
            this.CreateRoom.Location = new System.Drawing.Point(186, 208);
            this.CreateRoom.Name = "CreateRoom";
            this.CreateRoom.Size = new System.Drawing.Size(219, 44);
            this.CreateRoom.TabIndex = 3;
            this.CreateRoom.UseVisualStyleBackColor = false;
            this.CreateRoom.Click += new System.EventHandler(this.CreateRoom_Click);
            // 
            // MyStatus
            // 
            this.MyStatus.BackColor = System.Drawing.Color.Transparent;
            this.MyStatus.BackgroundImage = global::Client.Properties.Resources.statuss;
            this.MyStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MyStatus.Location = new System.Drawing.Point(186, 267);
            this.MyStatus.Name = "MyStatus";
            this.MyStatus.Size = new System.Drawing.Size(219, 39);
            this.MyStatus.TabIndex = 4;
            this.MyStatus.UseVisualStyleBackColor = false;
            this.MyStatus.Click += new System.EventHandler(this.MyStatus_Click);
            // 
            // BestScore
            // 
            this.BestScore.BackColor = System.Drawing.Color.Transparent;
            this.BestScore.BackgroundImage = global::Client.Properties.Resources.bestScore;
            this.BestScore.Location = new System.Drawing.Point(186, 312);
            this.BestScore.Name = "BestScore";
            this.BestScore.Size = new System.Drawing.Size(219, 42);
            this.BestScore.TabIndex = 5;
            this.BestScore.UseVisualStyleBackColor = false;
            this.BestScore.Click += new System.EventHandler(this.BestScore_Click);
            // 
            // SignOut
            // 
            this.SignOut.BackColor = System.Drawing.Color.Transparent;
            this.SignOut.BackgroundImage = global::Client.Properties.Resources.signOut;
            this.SignOut.Location = new System.Drawing.Point(186, 369);
            this.SignOut.Name = "SignOut";
            this.SignOut.Size = new System.Drawing.Size(221, 43);
            this.SignOut.TabIndex = 6;
            this.SignOut.UseVisualStyleBackColor = false;
            this.SignOut.Click += new System.EventHandler(this.SignOut_Click);
            // 
            // navigateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.Untitled_1;
            this.ClientSize = new System.Drawing.Size(534, 461);
            this.Controls.Add(this.SignOut);
            this.Controls.Add(this.BestScore);
            this.Controls.Add(this.MyStatus);
            this.Controls.Add(this.CreateRoom);
            this.Controls.Add(this.JoinRoom);
            this.Controls.Add(this.HelloUser);
            this.Name = "navigateForm";
            this.Text = "navigateForm";
            this.Load += new System.EventHandler(this.navigateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HelloUser;
        private System.Windows.Forms.Button JoinRoom;
        private System.Windows.Forms.Button CreateRoom;
        private System.Windows.Forms.Button MyStatus;
        private System.Windows.Forms.Button BestScore;
        private System.Windows.Forms.Button SignOut;
    }
}