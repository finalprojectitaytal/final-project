﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;

namespace Client
{
    public partial class navigateForm : Form
    {
        private string username;
        private NetworkStream clientStream;
        private bool check;

        private bool temp;

        public navigateForm(string username, NetworkStream clientStream)
        {
            InitializeComponent();
            this.username = username;
            this.clientStream = clientStream;

            HelloUser.Text =username;

            this.temp = false;

            Thread listening = new Thread(Listening);

            listening.Start();

            
        }

        public navigateForm(string username, NetworkStream clientStream,bool temp)
        {
            InitializeComponent();
            this.username = username;
            this.clientStream = clientStream;

            HelloUser.Text = username;

            this.temp = true;

        }

        public void changeCheck()
        {
            Thread listening = new Thread(Listening);

            listening.Start();
        }

        private void Listening()
        {

            this.check = true;

            while (check)
            {
                try
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead = clientStream.Read(buffer, 0, 4096);
                    Array.Resize<byte>(ref buffer, bytesRead);
                    string input = new ASCIIEncoding().GetString(buffer);


                    string msgCode = input.Substring(0, 3);

                    //לטפל בהודעות
                    
                    if (msgCode.Equals("126")) //Personal Status
                    {
                        Form myStatus = new MyStatus(this, input,this.clientStream);
                        Invoke((MethodInvoker)delegate { this.Hide(); });

                        Invoke((MethodInvoker)delegate { myStatus.Show(); });
                    }
                    else if(msgCode.Equals("106"))  //request room
                    {
                        join_room(input);
                    }
                    else if(msgCode.Equals("124"))  //best score
                    {
                        Form best = new BestScores(this, input,this.clientStream);
                        Invoke((MethodInvoker)delegate { this.Hide(); });

                        Invoke((MethodInvoker)delegate { best.Show(); });
                    }

                }
                catch
                {

                }
            }

        }

        private void navigateForm_Load(object sender, EventArgs e)
        {

        }

        private void MyStatus_Click(object sender, EventArgs e)
        {
            if(this.temp)
            {
                Thread listening = new Thread(Listening);

                listening.Start();
            }

            string message = "225";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }

        private void JoinRoom_Click(object sender, EventArgs e)
        {
            if (this.temp)
            {
                Thread listening = new Thread(Listening);

                listening.Start();
            }
            string message = "205";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }

        private void join_room(string message)
        {


            this.check = false;

            string[,] rooms;

            int index = 3;

            int roomNum = int.Parse(message.Substring(index, 4));

            index = index + 4;

            rooms = new string[roomNum, 2];

            for (int i = 0; i < roomNum; i++)
            {
                string roomID = message.Substring(index, 4);
                index = index + 4;

                int roomLen = int.Parse(message.Substring(index, 2));
                index = index + 2;

                string roomName = message.Substring(index, roomLen);
                index = index + roomLen;

                rooms[i, 0] = roomID;
                rooms[i, 1] = roomName;

               
            }

            //cell to join room form
            Form roomForm = new RoomMenu(rooms,this.clientStream,this.username);

            Invoke((MethodInvoker)delegate { this.Hide(); });

            Invoke((MethodInvoker)delegate { roomForm.Show(); });

            Invoke((MethodInvoker)delegate { this.Close(); });

        }

        private void BestScore_Click(object sender, EventArgs e)
        {
            if (this.temp)
            {
                Thread listening = new Thread(Listening);

                listening.Start();
            }

            string message = "223";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }

        private void CreateRoom_Click(object sender, EventArgs e)
        {
            this.check = false;

            Form createRoom = new CreateRoom(this.username, this.clientStream);

            this.Hide();

            createRoom.Show();

            this.Close();
        }

        private void SignOut_Click(object sender, EventArgs e)
        {
            this.check = false;

            Form signIn = new Form1();

            string message = "201";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();

            this.Hide();

            signIn.Show();

            this.Close();
        }

        private void navigateForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string message = "201";

            byte[] buffer = new ASCIIEncoding().GetBytes(message);

            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
        }
    }
}
