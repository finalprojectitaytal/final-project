#include "DataBase.h"

///Done by Itay

unordered_map<string, vector<string>> results;

/*
Function is the c'tor of the DataBase
Input: none
Output: none
*/
DataBase::DataBase()
{
	char *zErrMsg = 0;
	int rc;
	rc = sqlite3_open(DATABASE_NAME, &this->_db);
	if (rc)
	{
		throw("Can't open the datebase");
	}

	//Creating the 4 tables
	rc = sqlite3_exec(this->_db, "CREATE TABLE t_users(username text primarykey not null, password text not null, email text not null)", callback, 0, &zErrMsg);
	rc = sqlite3_exec(this->_db, "CREATE TABLE t_games(game_id integer primary key autoincrement not null, status integer not null, start_time DATETIME not null, end_time DATETIME)", callback, 0, &zErrMsg);
	rc = sqlite3_exec(this->_db, "CREATE TABLE t_questions(question_id integer primary key autoincrement not null, question text not null, correct_ans text not null, ans2 text not null, ans3 text not null, ans4 text not null, category text not null)", callback, 0, &zErrMsg);
	rc = sqlite3_exec(this->_db, "CREATE TABLE t_players_answers(game_id integer not null, username text not null, question_id integer not null, player_answer text not null, is_correct integer not null, answer_time integer not null, primary key(game_id, username, question_id), foreign key(game_id) REFERENCES t_games(game_id), foreign key(username) REFERENCES t_users(username), foreign key(question_id) REFERENCES t_questions(question_id))", callback, 0, &zErrMsg);	
}

/*
Function is the d'tor of the DataBase
Input: none
Output: none
*/
DataBase::~DataBase()
{
	sqlite3_close(this->_db);
}

/*
Function is a support function
*/
int DataBase::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

/*
Function is a support function
*/
void DataBase::clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

/*
Function will check if a user exists in the database
Input: the username
Output: true or false
*/
bool DataBase::isUserExists(string username)
{
	int rc;
	char *zErrMsg = 0;
	bool result;
	string message = "select * from t_users where username = ";
	message = message + "\'" + username + "\'";
	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //Checking if the user exists or not
	{
		result = false;
	}
	else
	{
		result = true;
	}

	this->clearTable();

	return result;
}

/*
Function will add a new user to the database
Input: the username, the password, the email
Output: true or false
*/
bool DataBase::addNewUser(string username, string password, string email)
{
	int rc;
	char *zErrMsg = 0;
	string message = "insert into t_users (username, password, email) values(";
	message = message + "\'" + username + "\'";
	message = message + ", " + "\'" + password + "\'";
	message = message + ", " + "\'" + email + "\'" + ")";


	if (this->isUserExists(username)) //Checking if a user with the username that was given to us already exists
	{
		return false;
	}

	rc = sqlite3_exec(this->_db, message.c_str(), callback, NULL, &zErrMsg);

	return true;
}

/*
Function will check if there is a someone with both the username and the password that was given to us
Input: the username and the password
Output: true or false
*/
bool DataBase::isUserAndPassMatch(string username, string password)
{
	int rc;
	char *zErrMsg = 0;
	string message = "select * from t_users where username = ";
	message = message + "\'" + username + "\'";
	message = message + " and password = ";
	message = message + "\'" + password + "\'";

	bool result;

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //Checking if there is no match
	{
		result = false;
	}
	else
	{
		result = true;
	}

	this->clearTable();

	return result;
}

/*
Function will return a number of quesitons randomly
Input: the number of questions
Output: a vector of the randomised questions
*/
vector<Question*> DataBase::initQuestions(int questionsNo, string category)
{
	int rc;
	char *zErrMsg = 0;
	vector<Question*> questions;
	int randomId;
	const int temp = questionsNo;
	bool* checkIfChosen;
	string message;
	int amountOfQuestionsInDb;
	int id;
	string question;
	string correct_answer;
	string ans2;
	string ans3;
	string ans4;
	
	if (category._Equal(ALL)) //Checking if we can send all the categories
	{
		rc = sqlite3_exec(this->_db, "select * from t_questions", callback, 0, &zErrMsg);
		amountOfQuestionsInDb = results.find("question_id")->second.size();
		checkIfChosen = new bool[amountOfQuestionsInDb];

		if (results.find("question_id")->second.size() < questionsNo)
		{
			throw("You chose too much questions");
		}
		else
		{
			for (int i = 0; i < amountOfQuestionsInDb; i++)
			{
				checkIfChosen[i] = false;
			}

			for (int j = 0; j < questionsNo; j++)
			{
				randomId = std::rand() % amountOfQuestionsInDb + 1;

				while (checkIfChosen[randomId - 1])
				{
					randomId = std::rand() % amountOfQuestionsInDb + 1;
				}

				checkIfChosen[randomId - 1] = true;

				message = "select question_id, question, correct_ans, ans2, ans3, ans4 from t_questions limit " + std::to_string(randomId - 1) + ", 1";

				this->clearTable();
				rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

				for (auto it = results.begin(); it != results.end(); it++) //For the future me, 
																		   //Hey it's me Mario. Please Check this shit if you have an ERROR
				{
					if (it->first._Equal(QUESTION_ID))
					{
						id = atoi(it->second[0].c_str());
					}
					else if (it->first._Equal(QUESTION))
					{
						question = it->second[0];
					}
					else if (it->first._Equal(CORRECT_ANS))
					{
						correct_answer = it->second[0];
					}
					else if (it->first._Equal(ANS2))
					{
						ans2 = it->second[0];
					}
					else if (it->first._Equal(ANS3))
					{
						ans3 = it->second[0];
					}
					else
					{
						ans4 = it->second[0];
					}
				}

				Question* qst = new Question(id, question, correct_answer, ans2, ans3, ans4);

				questions.push_back(qst);

				this->clearTable();
			}
		}
	}
	else
	{
		message = "select * from t_questions where category = ";
		message = message + "\'" + category + "\'";

		rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);
		amountOfQuestionsInDb = results.find("question_id")->second.size();
		checkIfChosen = new bool[amountOfQuestionsInDb];

		if (results.find("question_id")->second.size() < questionsNo)
		{
			throw("You chose too much questions");
		}
		else
		{
			for (int i = 0; i < amountOfQuestionsInDb; i++)
			{
				checkIfChosen[i] = false;
			}

			for (int j = 0; j < questionsNo; j++)
			{
				randomId = std::rand() % amountOfQuestionsInDb + 1;

				while (checkIfChosen[randomId - 1])
				{
					randomId = std::rand() % amountOfQuestionsInDb + 1;
				}

				checkIfChosen[randomId - 1] = true;

				message = "select question_id, question, correct_ans, ans2, ans3, ans4 from t_questions where category = ";
				message = message + "\'" + category + "\'";
				message = message + "limit " + std::to_string(randomId - 1) + ", 1";

				this->clearTable();
				rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

				for (auto it = results.begin(); it != results.end(); it++) //For the future me, 
																		   //Hey it's me Mario. Please Check this shit if you have an ERROR
				{
					if (it->first._Equal(QUESTION_ID))
					{
						id = atoi(it->second[0].c_str());
					}
					else if (it->first._Equal(QUESTION))
					{
						question = it->second[0];
					}
					else if (it->first._Equal(CORRECT_ANS))
					{
						correct_answer = it->second[0];
					}
					else if (it->first._Equal(ANS2))
					{
						ans2 = it->second[0];
					}
					else if (it->first._Equal(ANS3))
					{
						ans3 = it->second[0];
					}
					else if (it->first._Equal(ANS4))
					{
						ans4 = it->second[0];
					}
				}

				Question* qst = new Question(id, question, correct_answer, ans2, ans3, ans4);

				questions.push_back(qst);

				this->clearTable();
			}
		}
	}

	return questions;
}

/*
Function will insert a new game to the database
Input: none
Output: the id of the new game
*/
int DataBase::insertNewGame()
{
	int rc;
	char *zErrMsg = 0;
	int id;

	//Creating the new game
	rc = sqlite3_exec(this->_db, "insert into t_games (status, start_time, end_time) values(0, datetime('NOW'), datetime('NOW'))", callback, NULL, &zErrMsg);
	this->clearTable();

	//Getting the id of the new game
	rc = sqlite3_exec(this->_db, "select game_id from t_games where game_id = last_insert_rowid()", callback, 0, &zErrMsg);
	id = atoi(results.begin()->second[0].c_str()); ///For the future me, in case of an ERROR check this shit
	this->clearTable();

	return id;
}

/*
Function will update the status to 1 and the end_time to NOW
Input: the id of the game
Output: true or false
*/
bool DataBase::updateGameStatus(int gameId)
{
	int rc;
	char *zErrMsg = 0;
	string message = "update t_games set status = 1, end_time = 'NOW' where game_id = " + std::to_string(gameId);

	try
	{
		rc = sqlite3_exec(this->_db, message.c_str(), callback, NULL, &zErrMsg);
		this->clearTable();
		return true;
	}
	catch (...)
	{

	}

	return false;
}

/*
Function will add an answer to a player
Input: the game id, the username, the question id, the answer, if the player has answered correctly or not, the the time it took the player to answer
Output: true or false
*/
bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	int rc;
	char *zErrMsg = 0;
	string message = "insert into t_players_answers (game_id, username, question_id, player_answer, is_correct, answer_time) values(";
	message = message + std::to_string(gameId) + ", ";
	message = message + "\'" + username + "\'" + ", ";
	message = message + std::to_string(questionId) + ", ";
	message = message + "\'" + answer + "\'" + ", ";

	if (isCorrect)
	{
		message = message + "1" + ", " + std::to_string(answerTime) + ")";
	}
	else
	{
		message = message + "0" + ", " + std::to_string(answerTime) + ")";
	}

	try
	{
		rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);
		return true;
	}
	catch(...)
	{

	}

	return false;
}

/*
Function will return the scores of all of the user ordered from high to low
Input: none
Output: the scores of all of the user ordered from high to low
*/
multimap<int, string> DataBase::getBestScores()
{
	int rc;
	char *zErrMsg = 0;
	map<string, int> scores;
	multimap<int, string> best;
	multimap<int, string> bestHighToLow;
	int i = 0;
	string message = "select username, is_correct from t_players_answers";

	if (results.size() > 0)
	{
		this->clearTable();
	}

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	auto users = results.find("username");
	auto outcome = results.find("is_correct");

	for (i = 0; i < users->second.size(); i++)
	{
		if (scores.find(users->second[i]) == scores.end()) //Checking if it is a new user
		{
			scores.insert(std::pair<string, int>(users->second[i], 0));
		}

		string isCorrect = outcome->second[i];

		if (isCorrect._Equal("1")) //Checking if the user is correct
		{
			scores.at(users->second[i])++;
		}
	}

	for (auto it = scores.begin(); it != scores.end(); it++) //Swaping the keys and values
	{
		best.insert(std::pair<int, string>(it->second, it->first));
	}

	std::multimap<int, string>::reverse_iterator rit;
	rit = best.rbegin();
	/*
	for (rit = best.rbegin(); rit != best.rend(); rit++) //Reversing the map
	{
		bestHighToLow.insert(std::pair<int, string>(rit->first, rit->second));
	}
	*/
	i = 0;

	while ((i < 3) && (rit != best.rend()))
	{
		bestHighToLow.insert(std::pair<int, string>(rit->first, rit->second));

		rit++;
		i++;
	}

	this->clearTable();

	return bestHighToLow;
}

/*
Function will return the personal score of a user
Input: the username
Output: the personal score of the user
Output: the personal score of the user
*/
vector<string> DataBase::getPersonalScore(string username)
{
	int rc;
	char *zErrMsg = 0;
	string message = "select game_id from t_players_answers where username = ";
	message = message + "\'" + username + "\'";
	vector<string> personalScore;
	vector<string> games;
	int rightAnswersCount = 0;
	int wrongAnswersCount = 0;
	float avgTimeForAnswer = 0.0;

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	try
	{
		auto gamesResults = results.find("game_id");	//check if size != 0

		for (unsigned int i = 0; i < gamesResults->second.size(); i++)
		{
			if (std::find(games.begin(), games.end(), gamesResults->second[i]) == games.end()) //Checking if the game id already exists
			{
				games.push_back(gamesResults->second[i]);
			}
		}

		personalScore.push_back(std::to_string(games.size())); //Adding the number of games to the vector

		this->clearTable();

		message = "select is_correct from t_players_answers where username = ";
		message = message + "\'" + username + "\'";

		rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

		auto ansResults = results.find("is_correct");

		for (unsigned int i = 0; i < ansResults->second.size(); i++) //Going throgh all the answers of the user
		{
			string ans = ansResults->second[i];

			if (ans._Equal("1")) //Checking if the answer was correct or no
			{
				rightAnswersCount++;
			}
			else
			{
				wrongAnswersCount++;
			}
		}

		personalScore.push_back(std::to_string(rightAnswersCount)); //Adding the number of right answers to the personal score
		personalScore.push_back(std::to_string(wrongAnswersCount)); //Adding the number of wrong answers to the personal score

		this->clearTable();

		message = "select answer_time from t_players_answers where username = ";
		message = message + "\'" + username + "\'";

		rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

		auto ansTime = results.find("answer_time");

		avgTimeForAnswer = atoi(ansTime->second[0].c_str());

		for (unsigned int i = 1; i < ansTime->second.size(); i++) //Change the way you calculate avg
		{
			avgTimeForAnswer = (avgTimeForAnswer + atoi(ansTime->second[i].c_str()));
		}

		avgTimeForAnswer = avgTimeForAnswer / ansTime->second.size();

		personalScore.push_back(std::to_string(avgTimeForAnswer)); //Adding the average time to answer to the personal score
		this->clearTable();
	}
	catch (...)
	{
		vector<string> temp;
		return temp;
	}
	return personalScore;
}