#pragma once
#ifndef DATABASE_H
#define DATABASE_H

///Done by Itay

#include "sqlite3.h"
#include "Question.h"
#include <string>
#include <vector>
#include <exception>
#include <unordered_map>
#include <map>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <time.h>

#define DATABASE_NAME "ServerDataBase.db"
#define QUESTION_ID "question_id"
#define	QUESTION "question"
#define CORRECT_ANS "correct_ans"
#define ANS2 "ans2"
#define ANS3 "ans3"
#define ANS4 "ans4"
#define ALL "all"

using namespace std;
//unordered_map<string, vector<string>> results;

class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(string username);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password);
	vector<Question*> initQuestions(int questionsNo, string category);
	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);
	multimap<int, string> getBestScores();
	vector<string> getPersonalScore(string username);

	//static int callbackCount(void*, int, char**, char**);
	//static int callbackQuestions(void*, int, char**, char**);
	//static int callbackBestScores(void*, int, char**, char**);
	//static int callbackPersonalStatus(void*, int, char**, char**);
private:
	sqlite3* _db;
	static int callback(void* notUsed, int argc, char** argv, char** azCol);
	static void clearTable();
};

#endif // !DATABASE_H
