#include "Game.h"

///Done by Itay

/*
Function is the c'tor of the Game
Input: the players, the number of questions and the database
Output: none
*/
Game::Game(const vector<User*>& players, int questionsNo, string category, DataBase& db) : _db(db)
{
	this->_id = this->_db.insertNewGame();
	if (this->_id == MINUS)
	{
		throw("ERROR");
	}

	this->_questions = this->_db.initQuestions(questionsNo, category);
	this->_players = players;
	this->_currQuestionIndex = 0;
	this->_questionsNo = questionsNo;

	for (auto it = this->_players.begin(); it != this->_players.end(); it++) //nullifing the results
	{
		this->_results.insert(std::pair<string, int>((*it)->getUsername(), ZERO));
	}

	for (auto it = this->_players.begin(); it != this->_players.end(); it++) //Setting the game for all of the users
	{
		(*it)->setGame(this);
	}

	for (unsigned int i = 0; i < this->_players.size(); i++) //Reseting the scores of the users
	{
		this->_players[i]->resetScore();
	}
}

/*
Function is the d'tor of the Game
Input: none
Output: none
*/
Game::~Game()
{
	for (unsigned int i = 0; i < this->_questions.size(); i++) ///There is a chance that this will be an ERROR
	{
		delete(this->_questions[i]);
	}

	this->_questions.clear();
	this->_players.clear();
}

/*
function will send the question to all of the players
Input: none
Output: none
*/
void Game::sendQuestionToAllUsers()
{
	string message;

	if (this->_questions[this->_currQuestionIndex]->getQuestion().length() == 0) //Checking if there is a question to send
	{
		message = SEND_QUESTION_ERROR;
	}
	else
	{
		message = SEND_QUESTION + Helper::getPaddedNumber(this->_questions[this->_currQuestionIndex]->getQuestion().length(), THREE) + this->_questions[this->_currQuestionIndex]->getQuestion();
		//string* answers = new string[4];
		//answers = this->_questions[this->_currQuestionIndex]->getAnswers();
		vector<string> answers = this->_questions[this->_currQuestionIndex]->getAnswers();
		int size;
		string ans;

		for (unsigned int i = 0; i < answers.size(); i++)
		{
			ans = answers[i];
			size = answers[i].length();

			message = message + Helper::getPaddedNumber(size, THREE) + ans;
		}
	}

	this->_currentTurnAnswers = 0;

	for (unsigned int i = 0; i < this->_players.size(); i++) //Sending to all of the players
	{
		try
		{
			this->_players[i]->send(message);
		}
		catch (...)
		{

		}
	}
}

/*
Function will finish the game
Input: none
Output: none
*/
void Game::handleFinishGame()
{
	string message;

	try
	{
		this->_db.updateGameStatus(this->_id);

		message = END_GAME + Helper::getPaddedNumber(this->_players.size(), 1);

		for (unsigned int i = 0; i < this->_players.size(); i++) //Sending the "end game" message to all of the players
		{
			message = message + Helper::getPaddedNumber(this->_players[i]->getUsername().length(), 2);
			message = message + this->_players[i]->getUsername();
			message = message + Helper::getPaddedNumber(this->_players[i]->getScore(), 2);
		}

		for (unsigned int i = 0; i < this->_players.size(); i++)
		{
			this->_players[i]->send(message);
			this->_players[i]->clearGame();
		}
	}
	catch (...)
	{

	}
}

/*
Function will send the first question
Input: none
Output: none
*/
void Game::sendFirstQuestion()
{
	this->sendQuestionToAllUsers();
}

/*
Function will handle the turn
Input: none
Output: true or false, if the game is still going or not
*/
bool Game::handleNextTurn()
{
	if (this->_players.size() == ZERO) //Checking if there are any players in the game
	{
		this->handleFinishGame();
		return false;
	}

	if (this->_currentTurnAnswers == this->_players.size()) //Checking if all of the players have answered the question
	{
		if (this->_currQuestionIndex == this->_questionsNo - 1) //Checking if the question was the last one
		{
			this->handleFinishGame();
			return false;
		}

		this->_currQuestionIndex++;
		this->sendQuestionToAllUsers(); //Sending the next question to the users
	}

	return true;
}

/*
Function will handle the answer of the user
Input: the user, the number of the answer and the time it took the user to answer
Output: true or false, if the game is still going or not
*/
bool Game::handleAnswerFromUser(User* user, int answerNo, int time)

{
	bool correct = false;
	vector<string> answers = this->_questions[this->_currQuestionIndex]->getAnswers();
	this->_currentTurnAnswers++;

	if ((answerNo - 1) == this->_questions[this->_currQuestionIndex]->getCorrectAnswerIndex()) //Checking if the answer is correct
	{
		for (auto it = this->_results.begin(); it != this->_results.end(); it++)
		{
			if (user->getUsername()._Equal(it->first)) //Searching for the user
			{
				it->second++; //Raising the score of the user by 1
				user->upScore();
			}
		}

		correct = true;
	}

	if (answerNo == DID_NOT_ANSWER) //Adding the answer of the user to the database
	{
		this->_db.addAnswerToPlayer(this->_id, user->getUsername(), this->_questions[this->_currQuestionIndex]->getId(), "", correct, time);
	}
	else
	{
		this->_db.addAnswerToPlayer(this->_id, user->getUsername(), this->_questions[this->_currQuestionIndex]->getId(), answers[answerNo - 1], correct, time);
	}
	
	if (correct) //Sending to the user the result of his answer
	{
		string message = ANSWER_RESULT + std::to_string(1);

		user->send(message);
	}
	else
	{
		string message = ANSWER_RESULT + std::to_string(0);

		user->send(message);
	}

	return (this->handleNextTurn());
}

/*
Function will remove the user that asked to leave
Input: the user that asked to leave
Output: true or false, if the game is still going or not
*/
bool Game::leaveGame(User* currUser)
{
	for (unsigned int i = 0; i < this->_players.size(); i++) //Searching for the user
	{
		if (this->_players[i]->getUsername()._Equal(currUser->getUsername()))
		{
			this->_players.erase(this->_players.begin(), this->_players.begin() + i); //Removing the user from the list
		}
	}

	return (this->handleNextTurn());
}

/*
Function will return the id of the game
Input: none
Output: the id of the game
*/
int Game::getId()
{
	return this->_id;
}

/*
Function will return the results of all of the users
Input: none
Output: the results of all of the users
*/
map<string, int> Game::getResults()
{
	return this->_results;
}