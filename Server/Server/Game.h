#pragma once
#ifndef GAME_H
#define GAME_H

#include "Question.h"
#include "User.h"
#include "DataBase.h"
#include "Protocol.h"
#include "Helper.h"
#include <vector>
#include <map>
#include <string>

#define MINUS -1
#define ZERO 0
#define THREE 3
#define DID_NOT_ANSWER 5

///Done by Itay

using std::vector;
using std::map;
using std::string;

class User;

class Game
{
public:
	Game(const vector<User*>& players, int questionsNo, string category, DataBase& db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* currUser);
	int getId();
	map<string,int> getResults();

private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questionsNo;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	unsigned int _currentTurnAnswers;
	int _id;

	void sendQuestionToAllUsers();
};

#endif // !GAME_H
