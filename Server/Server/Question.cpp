#include "Question.h"

///Done by Itay

/*
Function is the c'tor of the Question
Input: the question id, the question, the correct answer, and other 3 answers
Output: none
*/
Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	this->_id = id;
	this->_question = question;
	int i = 0;
	int index;

	//srand(time(NULL));

	for (i = 0; i < NUM_OF_ANSWERS; i++) //Resetting the the answers
	{
		this->_answers.push_back("");
	}

	for (int i = 0; i < NUM_OF_ANSWERS; i++)
	{
		index = std::rand() % NUM_OF_ANSWERS;

		while (this->_answers[index] != "") //Checking if the index wasn't chosen yet
		{
			index = std::rand() % NUM_OF_ANSWERS;
		}

		if (i == 0)
		{
			this->_answers[index] = correctAnswer;
			this->_correctAnswerIndex = index;
		}
		else if (i == 1)
		{
			this->_answers[index] = answer2;
		}
		else if (i == 2)
		{
			this->_answers[index] = answer3;
		}
		else
		{
			this->_answers[index] = answer4;
		}
	}
}

/*
Function is the d'tor of the Question
Input: none
Output: none
*/
Question::~Question()
{
	///There is nothing to do here
}

/*
Function will return the question
Input: none
Output: the question
*/
string Question::getQuestion()
{
	return this->_question;
}

/*
Function will return the answers
Input: none
Output: the answers
*/
vector<string> Question::getAnswers()
{
	return this->_answers;
}

/*
Function will return the index of the correct answer
Input: none
Output: the index of the correct answer
*/
int Question::getCorrectAnswerIndex()
{
	return this->_correctAnswerIndex;
}

/*
Function will return the id of the question
Input: none
Output: the id of the question
*/
int Question::getId()
{
	return this->_id;
}