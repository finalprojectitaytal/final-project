#pragma once
#ifndef QUESTION_H
#define QUESTION_H

#include <time.h>
#include <string>
#include <vector>

///Done by Itay

#define NUM_OF_ANSWERS 4

using std::string;
using std::vector;

class Question
{
public:
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);
	~Question();
	string getQuestion();
	vector<string> getAnswers();
	int getCorrectAnswerIndex();
	int getId();

private:
	string _question;
	//string _answers[NUM_OF_ANSWERS];
	vector<string> _answers;
	int _correctAnswerIndex;
	int _id;
};


#endif // !QUESTION_H
