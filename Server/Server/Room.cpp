#include "Room.h"

/*
	The d'tor of room.
*/
Room::Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime,string category)
{
	this->_id = id;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_questionNo = questionsNo;
	this->_questionTime = questionTime;
	this->_users.push_back(admin);
	this->category = category;
}

/*
	The d'tor of room.
*/
Room::~Room()
{

}

/*
	The function return all user that in the room;
	Input: vector of users and pointer to user
	Output: string of all users names in the room.
*/
string Room::getUsersAsString(vector<User*> usersList, User* excludeUser)
{
	string temp = "";

	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		if (!(this->_users[i]->getUsername()._Equal(excludeUser->getUsername())))
		{
			temp = temp + " ," + this->_users[i]->getUsername();
		}
	}

	return temp;
}


/*
	The function return the 108 message.
	Input: none
	Output: 108 message
*/
string Room::getUsersListMessage()
{
	string message = SEND_USERS + std::to_string(this->_users.size());
	int temp;

	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		temp = this->_users[i]->getUsername().length();

		if (temp < 9 && temp > 0)
			message = message + "0" + std::to_string(temp) + this->_users[i]->getUsername();
		else
			message = message + std::to_string(temp) + this->_users[i]->getUsername();

	}

	return message;
}

/*
	The function send message that received from one user in the room to all users in the room.
	Input: Pointer to user that sent the message and the message to send.
	Output: none.
*/
void Room::sendMessage(User* excludeUser, string message)
{
	if (excludeUser != NULL)
	{
		for (unsigned int i = 0; i < this->_users.size(); i++)
		{
			if (!(this->_users[i]->getUsername()._Equal(excludeUser->getUsername())))
			{
				try
				{
					this->_users[i]->send(message);
				}
				catch (...)
				{

				}
			}
		}
	}
	else
	{
		for (unsigned int i = 0; i < this->_users.size(); i++)
		{
			try
			{
				this->_users[i]->send(message);
			}
			catch (...)
			{

			}
			
		}
	}
}

/*
	The function send message  all users in the room.
	Input: message to send.
	Output: none.
*/
void Room::sendMessage(string message)
{
	sendMessage(NULL, message);
}

/*
	The function add user to the room if he have place in the room.
	Input: Pointer to user that want to join.
	Output: true or false if he success to join or no.
*/
bool Room::joinRoom(User* user)
{														
	if (this->_users.size() >= (unsigned int)this->_maxUsers)			
	{
		user->send(JOIN_ROOM_FAILED_ROOM_IS_FULL);
		return false;	

	}
	try
	{
		this->_users.push_back(user);
		string message = JOIN_ROOM_SUCCESS;

		if (_questionNo > 0 && _questionNo < 10)
		{
			message = message + "0" + std::to_string(this->_questionNo);
		}
		else
		{
			message = message + std::to_string(this->_questionNo);
		}

		if (_questionTime > 0 && _questionTime < 10)
		{
			message = message + "0" + std::to_string(this->_questionTime);
		}
		else
		{
			message = message + std::to_string(this->_questionTime);
		}
		user->send(message);
		sendMessage(getUsersListMessage());

		return true;
	}
	catch(...)
	{

	}

	return false;
}

/*
	The function delete user from the room.
	Input: user that want exit
	Output: none
*/
void Room::leaveRoom(User* user)
{

	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i]->getUsername()._Equal(user->getUsername()))
		{
			this->_users.erase(this->_users.begin() + i);
			user->send(LEAVE_ROOM_SUCCESS);
			sendMessage(getUsersListMessage());
		}
	}

}

/*
	The function close the room only if the admin press close room
	Input: user that want close the room.
	Output: -1 if fail or roomID if success
*/
int Room::closeRoom(User* user)
{
	if (this->_admin->getUsername()._Equal(user->getUsername()))
	{
		for (unsigned int i = 0; i < this->_users.size(); i++)
		{
			this->_users[i]->send(CLOSE_ROOM_RESPONSE);

			if (!(this->_users[i]->getUsername()._Equal(this->_admin->getUsername())))
			{
				this->_users[i]->clearRoom();
			}
		}

		this->_admin->clearRoom();

		int id = this->_id;
		delete this;			// need to check.
		return id;
	}

	return -1;
}


/*
	The function return the room id
	Input: none.
	Output: room id
*/
int Room::getRoomID()
{
	return this->_id;
}

/*
	The function return the room name.
	Input: none
	Output: room name
*/
string Room::getRoomName()
{
	return this->_name;
}

/*
	The function return the category
	Input: none.
	Output: category
*/
string Room::getCategory()
{
	return this->category;
}

/*
	The function return users vector
	Input: none
	Output: user vector
*/
vector<User*> Room::getUsers()
{
	return this->_users;
}

/*
The function return question number
Input: none
Output: number of question
*/
int Room::getQuestionNo()
{
	return this->_questionNo;
}

/*
The function return question time
Input: none
Output: time of question
*/
int Room::getQuestionTime()
{
	return this->_questionTime;
}

/*
The function return max users number
Input: none
Output: max users number
*/
int Room::getMaxUsers()
{
	return this->_maxUsers;
}