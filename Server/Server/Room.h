#pragma once
#include <vector>
#include <string>

#include "User.h"
#include "Protocol.h"

using std::vector;
using std::string;

class User;

class Room
{
private:
	vector<User*> _users;	
	User* _admin;	
	
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
	string category;

public:	
	Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime,string category);
	~Room();


	string getUsersAsString(vector<User*> usersList, User* excludeUser);

	string getUsersListMessage();

	void sendMessage(User* excludeUser,string message);
	void sendMessage(string message);

	bool joinRoom(User* user);

	void leaveRoom(User* user);

	int closeRoom(User* user);

	int getRoomID();
	vector<User*> getUsers();
	string getRoomName();
	string getCategory();

	int getQuestionNo();
	int getQuestionTime();
	int getMaxUsers();

};