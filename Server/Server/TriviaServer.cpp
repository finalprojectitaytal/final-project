#include "TriviaServer.h"

// Done by Tal.	 and two function by Itay.//

/*
	The c'tor of TrivaServer
*/
TriviaServer::TriviaServer()
{
	//cell DB c'tor
	this->_db = new DataBase();

	//open socket...
	// notice that we step out to the global namespace
	// for the resolution of the function socket
	this->_listeningSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_listeningSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


/*
	The d'tor of TrivaServer
*/
TriviaServer::~TriviaServer()
{
	TRACE(__FUNCTION__ " closing accepting socket");
	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(this->_listeningSocket);
	}
	catch (...) {}
}

/*
	The function make thread for listening to clients that want to join.
	Input: none.
	Output: none.
*/
void TriviaServer::serve()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TriviaServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		acceptClient();
	}
}

/*
	The function listening to join request to the server.
	Input: none.
	Output: none.
*/
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(this->_listeningSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(this->_listeningSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");

}

/*
	The function accept client that join and make thread for the client.
	Input: none.
	Output: none
*/
void TriviaServer::acceptClient()
{
	SOCKET client_socket = accept(this->_listeningSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}

/*
	The function get the message from the client and call function to add the message to queue.
	Input: client socket.
	Output: none.
*/
void TriviaServer::clientHandler(SOCKET client_socket)
{
	RecievedMessage* currMsg = nullptr;
	try
	{
		int messageCode = Helper::getMessageTypeCode(client_socket);

		while (messageCode != 0 && messageCode != atoi(EXIT_APP))
		{
			

			RecievedMessage* currMsg = buildRecieveMessage(client_socket, messageCode);

			
			addRecieveMessage(currMsg);

			messageCode = Helper::getMessageTypeCode(client_socket);

			std::cout << std::to_string(messageCode) << std::endl;
		}

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		currMsg = buildRecieveMessage(client_socket, atoi(EXIT_APP));
		addRecieveMessage(currMsg);
	}
	closesocket(client_socket);
}
/*
	The function take message from message queue and handling message.
	Input: none.
	Output: none.
*/
void TriviaServer::handleRecievedMessages()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	RecievedMessage* currMessage = nullptr;

	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMessages);

			// Wait for clients to enter the queue.
			if (_queRcvMessages.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (_queRcvMessages.empty())
				continue;

			RecievedMessage* currMessage = _queRcvMessages.front();
			_queRcvMessages.pop();
			lck.unlock();

			
			// Extract the data from the tuple.
			clientSock = currMessage->getSocket();

			currMessage->setUser(getUserBySocket(clientSock));

			msgCode = currMessage->getMessageID();

			string msgType = std::to_string(msgCode);
					//�� ��� ������� ���� ���� ����� �� ����.
					//���� ����� �������� ����� ������ ������.

			if (msgType.compare(SIGN_IN) == 0)			//Message 200
			{
				User* user = handleSignin(currMessage);

				if(user != nullptr)
					this->_connectedUsers[user->getSocket()] = user;
			}
			else if (msgType.compare(SIGN_OUT) == 0)	//Message 201
			{
				handleSignout(currMessage);
			}
			else if (msgType.compare(SIGN_UP) == 0)		//Message 203
			{
				handleSignup(currMessage);
			}
			else if (msgType.compare(GET_ROOMS) == 0)	//Messaeg 205
			{
				handleGetRooms(currMessage);
			}
			else if (msgType.compare(GET_USERS) == 0)	//Message 207
			{
				handleGetUsersInRoom(currMessage);
			}
			else if (msgType.compare(JOIN_ROOM) == 0)	//Message 209
			{
				handleJoinRoom(currMessage);
			}
			else if (msgType.compare(LEAVE_ROOM) == 0)	//Message 211
			{
				handleLeaveRoom(currMessage);
			}
			else if (msgType.compare(CREATE_ROOM) == 0)	//Message 213
			{
				handleCreateRoom(currMessage);
			}
			else if (msgType.compare(CLOSE_ROOM) == 0)	//Message 215
			{
				handleCloseRoom(currMessage);
			}
			else if (msgType.compare(BEGIN_GAME) == 0)	//Message 217
			{
				handleStartGame(currMessage);
			}
			else if (msgType.compare(SEND_ANSWER) == 0)	//Message 219
			{
				handlePlayerAnswer(currMessage);
			}
			else if (msgType.compare(LEAVE_GAME) == 0)	//Message 221
			{
				handleLeaveGame(currMessage);
			}
			else if (msgType.compare(BEST_SCORES) == 0)	//Message 223
			{
				handleGetBestScores(currMessage);
			}
			else if (msgType.compare(PERSONAL_STATUS) == 0)	//Message 225
			{
				handleGetPersonalStatus(currMessage);
			}
			else if (msgType.compare(EXIT_APP) == 0)		//Message 229
			{
				safeDeleteUesr(currMessage);
			}
			else
			{
				safeDeleteUesr(currMessage);
			}

			

			delete currMessage;
		}
		catch (...)
		{
			safeDeleteUesr(currMessage);
		}
	}
}

/*
	The function add message to message queue.
	Input: message to add.
	Output: none.
*/
void TriviaServer::addRecieveMessage(RecievedMessage* msg)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);

	_queRcvMessages.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();
}

/*
	The function get the room by the room id.
	Input: id of room.
	Output: room with this id or nullptr.
*/
Room* TriviaServer::getRoomById(int roomID)
{
	for (auto it = this->_roomsList.begin(); it != this->_roomsList.end(); it++)
	{
		if (it->first == roomID)
		{
			return it->second;
		}
	}

	return nullptr;
}


/*
	The function find the user by his name.
	Input: username.
	Output: user.
*/
User* TriviaServer::getUserByName(string username)
{
	for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if (it->second != nullptr)
		{
			if (it->second->getUsername().compare(username) == 0)
			{
				return it->second;
			}
		}
	}

	return nullptr;
}

/*
	The function get the user by the socket.
	Input: user socket.
	output: user.
*/
User* TriviaServer::getUserBySocket(SOCKET client_socket)
{
	for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if (it->first == client_socket)
		{
			return it->second;
		}
	}
	return nullptr;
}

/*
	The function delete user from the server.
	Input: client message.
	Output: none.
*/
void TriviaServer::safeDeleteUesr(RecievedMessage* msg)
{
	try
	{
		SOCKET user_sock = msg->getSocket();

		handleSignout(msg);

		this->_connectedUsers.erase(user_sock);
	}
	catch (...)
	{

	}
}

/*
	The funcion Sign in user and add him to the server, send to the user if success or fail
	Input: user message
	Output: pointer to user that added or nullptr.
*/
User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	User* user;
	string username = msg->getMessage()[0];
	string password = msg->getMessage()[1];

	bool flag = _db->isUserAndPassMatch(username, password);

	if (flag)	//username and password right.
	{
		user = getUserByName(username);

		if (user == nullptr)	//the user doesn't connected to the server
		{
			user = new User(username,msg->getSocket());
			this->_connectedUsers[msg->getSocket()] = user;

			Helper::sendData(msg->getSocket(), SIGN_IN_SUCCESS);

			return user;

		}
		else	//user already CONNECTED
		{
			Helper::sendData(msg->getSocket(), SIGN_IN_USER_IS_ALREADY_CONNECTED);
			user = nullptr;
		}

	}
	else	//username or password incorrect
	{
		Helper::sendData(msg->getSocket(), SIGN_IN_WRONG_DETAILS);
		user = nullptr;
	}

	return user;
}

/*
	The function SignUp the user to the database of the server.
	Input: user message to signup with all details.
	Output: true or false if success or fail.
*/
bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	string username = msg->getMessage()[0];
	string password = msg->getMessage()[1];
	string email = msg->getMessage()[2];
	
	if (isPasswordValid(password))	//check if the password is valid
	{
		if (isUsernameValid(username))		//check of the username valid
		{
			if (!this->_db->isUserExists(username))			//check if the username not Catch by anyone else
			{
				bool check = this->_db->addNewUser(username, password, email);

				if (check)
				{
					Helper::sendData(msg->getSocket(), SIGN_UP_SUCCESS);
					return true;
				}
				else
				{
					Helper::sendData(msg->getSocket(), SIGN_UP_OTHER);
					return false;
				}
			}
			else
			{
				Helper::sendData(msg->getSocket(), SIGN_UP_USERNAME_ALREADY_EXISTS);
				return false;
			}
		}
		else
		{
			Helper::sendData(msg->getSocket(), SIGN_UP_USERNAME_IS_ILLEGAL);
			return false;
		}
	}
	else
	{
		Helper::sendData(msg->getSocket(), SIGN_UP_PASS_ILLEGAL);
		return false;
	}

	return false;
}

/*
	The function SignOut user from the server.
	Input: user message to signout.
	Output: none.
*/
void TriviaServer::handleSignout(RecievedMessage* msg)
{
	User* user = msg->getUser();

	if (user != nullptr)
	{
		this->_connectedUsers.erase(user->getSocket());

		if (msg->getUser()->getRoom() != nullptr)
		{
			handleCloseRoom(msg);

			handleLeaveRoom(msg);
		}

		if (msg->getUser()->getGame() != nullptr)
		{
			handleLeaveGame(msg);
		}
	}
}


/*
	The function takes the user out of the game.
	Input: user message to leave the game.
	Output: none.
*/
void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	bool check = msg->getUser()->leaveGame();

	if (check)
	{
		Game* game = msg->getUser()->getGame();
		delete game;
	}
}

/*
	The function start the game.
	Input: user messgae to start game.
	Output: none.
*/
void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	Room* room = msg->getUser()->getRoom();

	try
	{
		Game* game = new Game(room->getUsers(), room->getQuestionNo(), room->getCategory(), *(this->_db));

		this->_roomsList.erase(room->getRoomID());

		game->sendFirstQuestion();
	}
	catch (...)
	{
		msg->getUser()->send(SEND_QUESTION_ERROR);
	}
}

/*
	The function get the player answer
	Input: user message with the answer of the question.
	Output: none.
*/
void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	Game* game = msg->getUser()->getGame();

	if (game != nullptr)
	{
		int answerNo = atoi(msg->getMessage()[0].c_str());
		int time = atoi(msg->getMessage()[1].c_str());
		
		bool check = game->handleAnswerFromUser(msg->getUser(), answerNo, time);

		if (check == false)
		{
			delete game;
		}
	}
}

/*
	The function create new room.
	Input: user message with room details
	Output: true or false if success or fail create room.
*/
bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	string roomName = msg->getMessage()[0];
	int playersNo = atoi(msg->getMessage()[1].c_str());
	int questionNo = atoi(msg->getMessage()[2].c_str());
	int questionTime = atoi(msg->getMessage()[3].c_str());
	string category = msg->getMessage()[4];

	this->_roomIdSequence++;

	if (msg->getUser()->createRoom((int)(this->_roomIdSequence), roomName, playersNo, questionNo, questionTime,category))
	{
		this->_roomsList[this->_roomIdSequence] = msg->getUser()->getRoom();
		return true;
	}

	return false;
}

/*
	The function close the room.
	Input: user messgae to close room.
	Output: true or false if success or fail close room.
*/
bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	int roomID = msg->getUser()->getRoom()->getRoomID();
	Room* room = msg->getUser()->getRoom();
	if (room != nullptr)
	{
		int check = msg->getUser()->closeRoom();

		if (check != -1)
		{
			this->_roomsList.erase(roomID);
			return true;
		}
	}
	return false;
}

/*
	The function add user to room.
	Input: user message to join room.
	Output: true or false id success or fail join room.
*/
bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	User* user = msg->getUser();

	if (user != nullptr)
	{
		int roomID = atoi((msg->getMessage())[0].c_str());

		Room* room = getRoomById(roomID);

		if (room != nullptr)
		{
			user->joinRoom(room);
			user->send(JOIN_ROOM_SUCCESS);

			for (unsigned int i = 0; i < room->getUsers().size(); i++)
			{
				room->getUsers()[i]->send(room->getUsersListMessage());

			}

			return true;
		}
		else
		{
			user->send(JOIN_ROOM_FAILED_ROOM_DOES_NOT_EXIST_OR_OTHER_REASON);
			return false;
		}
	}
	return false;
}

/*
	The function help user to leave room.
	Input: user message to leave room.
	Output: true or false if success or fail leave room.
*/
bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	if (msg->getUser() != nullptr)
	{
		if (msg->getUser()->getRoom() != nullptr)
		{
			

			for (unsigned int i = 0; i < msg->getUser()->getRoom()->getUsers().size(); i++)
			{
				if (msg->getUser()->getRoom()->getUsers()[i] != msg->getUser())
				{
					msg->getUser()->getRoom()->getUsers()[i]->send(msg->getUser()->getRoom()->getUsersListMessage());
				}
			}

			msg->getUser()->leaveRoom();
			return true;
		}
	}
	return false;
}

/*
	The function return all the user in the rooms
	Input: user messgae to get all users that connacted to the room.
	Output: none.
*/
void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	User* user = msg->getUser();

	int roomID = atoi((msg->getMessage())[0].c_str());

	Room* room = getRoomById(roomID);

	if (room != nullptr)
	{
		user->send(room->getUsersListMessage());
	}
	else
	{
		user->send(SEND_USERS + std::to_string(0));
	}
}

/*
	The function return list of all rooms.
	Input: user message to get all rooms
	Output: none.
*/
void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	User* user = msg->getUser();

	string message = SEND_ROOMS + Helper::getPaddedNumber(this->_roomsList.size(), 4);

	for (map<int, Room*>::iterator it = this->_roomsList.begin(); it != this->_roomsList.end(); it++)
	{
		message = message + Helper::getPaddedNumber(it->first, 4) + Helper::getPaddedNumber(it->second->getRoomName().length(),2) + it->second->getRoomName();
	}

	user->send(message);
}


/*
Function will send the top 3 best scores
Input: the recieved message
Output: none

Done by Itay

*/
void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	multimap<int, string> best = this->_db->getBestScores();
	string message = SEND_BEST_SCORES;

	/*for (int i = 0; i < 3; i++) //Working 3 times
	{
		if (best.begin() != best.end()) //Checking if there is a user to take
		{
			auto it = best.begin(); //Taking the first user
			message = message + Helper::getPaddedNumber((it->second.size()), 2) + it->second + Helper::getPaddedNumber(it->first, 6);

			best.erase(it); //Removing the user
		}
		else
		{
			message = message + Helper::getPaddedNumber(0, 2) + "" + Helper::getPaddedNumber(0, 6);
		}
	}*/

	if (best.size() == 0)
	{
		for (int i = 0; i < 3; i++)
		{
			message = message + Helper::getPaddedNumber((0), 2) + "" + Helper::getPaddedNumber(0, 6);
		}
	}
	else if (best.size() == 1)
	{
		std::multimap<int, string>::reverse_iterator rit;
		rit = best.rbegin();

		message = message + Helper::getPaddedNumber((rit->second.size()), 2) + rit->second + Helper::getPaddedNumber(rit->first, 6);

		for (int i = 0; i < 2; i++)
		{
			message = message + Helper::getPaddedNumber((0), 2) + "" + Helper::getPaddedNumber(0, 6);
		}
		
	}
	else if (best.size() == 2)
	{
		std::multimap<int, string>::reverse_iterator rit;
		rit = best.rbegin();

		for (int i = 0; i < 2; i++)
		{
			message = message + Helper::getPaddedNumber((rit->second.size()), 2) + rit->second + Helper::getPaddedNumber(rit->first, 6);

			rit++;
		}

		message = message + Helper::getPaddedNumber((0), 2) + "" + Helper::getPaddedNumber(0, 6);
	}
	else
	{
		std::multimap<int, string>::reverse_iterator rit;
		rit = best.rbegin();

		for (int i = 0; i < 3; i++)
		{
			message = message + Helper::getPaddedNumber((rit->second.size()), 2) + rit->second + Helper::getPaddedNumber(rit->first, 6);

			rit++;
		}
	}

	Helper::sendData(msg->getUser()->getSocket(), message);
}

/*
Function will send the user his personal status
Input: the recieved message
Output: none

Done by Itay

*/
void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	vector<string> status = this->_db->getPersonalScore(msg->getUser()->getUsername());

	if ((status.size() != 0) || (!(status[0]._Equal("")) && !(status[1]._Equal("")) && !(status[2]._Equal("")))) //Checking if the user played
	{
		//Adding the message code, the number of games, the number of right answers and the number of wrong answers to the message
		string message = SEND_PERSONAL_STATUS + Helper::getPaddedNumber(atoi(status[0].c_str()), 4) + Helper::getPaddedNumber(atoi(status[1].c_str()), 6) + Helper::getPaddedNumber(atoi(status[2].c_str()), 6);

		string time = status[3];

		int pointIndex = time.find_first_of('.');

		if (pointIndex != string::npos)
		{
			message = message + Helper::getPaddedNumber(atoi(time.substr(0, pointIndex).c_str()), 2) + Helper::getPaddedNumber(atoi(time.substr(pointIndex + 1, 2).c_str()), 2);
		}

		Helper::sendData(msg->getUser()->getSocket(), message); //Sending the message
	}
	else
	{
		Helper::sendData(msg->getUser()->getSocket(), "1260000"); //Sending the empty message
	}
}

/*
	The function build the RecievedMessage from message that user send.
	Input: user message.
	Output: RecievedMessage
*/
RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	RecievedMessage* msg = nullptr;
	vector<string> message;
	User* user = getUserBySocket(client_socket);

	if (std::to_string(msgCode).compare(SIGN_IN) == 0)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		int passwordSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passwordSize);

		

		message.push_back(userName);
		message.push_back(password);
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(SIGN_OUT) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(SIGN_UP) == 0)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string UserName = Helper::getStringPartFromSocket(client_socket, userSize);

		int passSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passSize);

		int emailSize = Helper::getIntPartFromSocket(client_socket, 2);
		string email = Helper::getStringPartFromSocket(client_socket, emailSize);

		message.push_back(UserName);
		message.push_back(password);
		message.push_back(email);

		user = new User(UserName, client_socket);

		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(GET_ROOMS) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(GET_USERS) == 0)
	{
		string roomID = Helper::getStringPartFromSocket(client_socket, 4);
		message.push_back(roomID);

		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(JOIN_ROOM) == 0)
	{
		string roomID = Helper::getStringPartFromSocket(client_socket, 4);
		message.push_back(roomID);

		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(LEAVE_ROOM) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(CREATE_ROOM) == 0)
	{
		int roomNameSize = Helper::getIntPartFromSocket(client_socket, 2);
		string roomName = Helper::getStringPartFromSocket(client_socket, roomNameSize);

		int playersNo = Helper::getIntPartFromSocket(client_socket, 1);

		int questionNo = Helper::getIntPartFromSocket(client_socket, 2);

		int questionTime = Helper::getIntPartFromSocket(client_socket, 2);

		int  categoryLen = Helper::getIntPartFromSocket(client_socket, 2);

		string category = Helper::getStringPartFromSocket(client_socket, categoryLen);

		message.push_back(roomName);
		message.push_back(std::to_string(playersNo));
		message.push_back(std::to_string(questionNo));
		message.push_back(std::to_string(questionTime));
		message.push_back(category);

		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(CLOSE_ROOM) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(BEGIN_GAME) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(SEND_ANSWER) == 0)
	{
		string answerNo = Helper::getStringPartFromSocket(client_socket, 1);
		string answerTime = Helper::getStringPartFromSocket(client_socket, 2);

		message.push_back(answerNo);
		message.push_back(answerTime);

		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(LEAVE_GAME) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(BEST_SCORES) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(PERSONAL_STATUS) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(EXIT_APP) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}

	return msg;
}