#pragma once
#include <vector>
#include <string>
#include <WinSock2.h>
#include <map>
#include <unordered_map>
#include <queue>
#include <exception>
#include <thread>
#include <iostream> 
#include <mutex>


#include "Room.h"
#include "RecievedMessage.h"
#include "Helper.h"
#include "DataBase.h"
#include "Validator.h"
#include "Game.h"

#pragma comment (lib, "Ws2_32.lib")

#define PORT 8820
#define IFACE 0

using std::string;
using std::vector;
using std::map;
using std::queue;
using std::thread;
using std::mutex;
using std::unique_lock;
using std::unordered_map;

class DataBase;
class Game;

class TriviaServer
{
private:
	map<SOCKET, User*> _connectedUsers;
	map<int, Room*> _roomsList;
	queue<RecievedMessage*> _queRcvMessages;
	int _roomIdSequence;

	DataBase* _db;

	SOCKET _listeningSocket;

	std::mutex _mtxRecievedMessages;
	std::condition_variable _msgQueueCondition;


	void bindAndListen();
	void acceptClient();

	void clientHandler(SOCKET client_socket);

	Room* getRoomById(int roomID);

	User* getUserByName(string username);

	User* getUserBySocket(SOCKET client_socket);

	void safeDeleteUesr(RecievedMessage* msg);

	User* handleSignin(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg);

	void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);

	bool handleCreateRoom(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg);
	bool handleJoinRoom(RecievedMessage* msg);
	bool handleLeaveRoom(RecievedMessage* msg);
	void handleGetUsersInRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);

	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);

	void handleRecievedMessages();		//todo
	void addRecieveMessage(RecievedMessage* msg);
	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);

public:
	TriviaServer();
	~TriviaServer();

	void serve();

};