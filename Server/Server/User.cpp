#include "User.h"

///Done by Itay

/*
Function is the c'tor of the user
Input: the username and the socket
Output: none
*/
User::User(string username, SOCKET sock) 
{
	this->_username = username;
	this->_sock = sock;
	this->_currRoom = nullptr;
	this->_currGame = nullptr;
	this->_bestScore = 0;
	this->_score = 0;
}

/*
Function is the d'tor of the user
Input: none
Output: none
*/
User::~User()
{
	//There is nothing to do here
}

/*
Function will send a message to the user
Input: the message
Output: none
*/
void User::send(string message)
{
	Helper::sendData(this->_sock, message);
}

/*
Function will set a new game
Input: the game
Output: none
*/
void User::setGame(Game* gm)
{
	this->_currRoom = nullptr;
	this->_currGame = gm;
}

/*
Function will stop the game
Input: none
Output: none
*/
void User::clearGame()
{
	this->_currGame = nullptr;
}

/*
Function will check if the user can create a room and if he can, it will create the room
Input: the room id, name, max users, number of questions, the time to answer every question
Output: true or false
*/
bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime,string category)
{
	if (this->_currRoom != nullptr) //Checking if the user is not in another room
	{
		this->send(CREATE_ROOM_FAIL);
		return false;
	}

	this->_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime,category);
	this->send(CREATE_ROOM_SUCCESS);
	this->send(CREATE_ROOM_SUCCESS);

	return true;
}

/*
Function will check if the user can join a room, if yes than it will join him
Input: the room to join
Output: true or false
*/
bool User::joinRoom(Room* newRoom)
{
	if (this->_currRoom != nullptr) //Checking if the user is already in another room
	{
		return false;
	}

	if (!(newRoom->joinRoom(this))) //Checking in the Room class if the user can join the room
	{
		return false;
	}

	this->_currRoom = newRoom; //Joinning the room

	return true;
}

/*
Function will check if the user can leave the room and if yes he will leave
Input: none
Output: none
*/
void User::leaveRoom()
{
	if (this->_currRoom != nullptr) //Checking if the user is in a room
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}
}

/*
Function will check if the user can close the room
Input: none
Output: the room id if the closing was successeful and -1 if not
*/
int User::closeRoom()
{
	if (this->_currRoom == nullptr) //Checking if the user belongs to a room
	{
		return MINUS;
	}

	return (this->_currRoom->closeRoom(this)); //The checking is in Room::closeRoom
}

/*
Function will make the user leave the game
Input: none
Output: true or false
*/
bool User::leaveGame()
{
	if (this->_currGame != nullptr) //Checking if the user is in a game
	{
		if (this->_currGame->leaveGame(this)) //Checking if the user was able to leave the game 
		{
			this->_currGame = nullptr;
			this->_currRoom = nullptr;
			return true;
		}
	}

	return false;
}

/*
Function will Change the room to nullptr
Input: none
Output: none
*/
void User::clearRoom()
{
	this->_currRoom = nullptr;
}

/*
Function will return the username
Input: none
Output: none
*/
string User::getUsername()
{
	return this->_username;
}

/*
Function will return the socket
Input: none
Output: the socket
*/
SOCKET User::getSocket()
{
	return this->_sock;
}

/*
Function will return the room
Input: none
Output: the room
*/
Room* User::getRoom()
{
	return this->_currRoom;
}

/*
Function will change the best score if we need to
Input: none
Output: none
*/
void User::setBestScore()
{
	if (this->_score > this->_bestScore) //Checking if we need to change the best score
	{
		this->_bestScore = this->_score;
	}
}

/*
Function will raise the score by 1
Input: none
Output: none
*/
void User::upScore()
{
	this->_score = this->_score + 1;
	this->setBestScore(); //Calling the function that will change the best score if necessary
}

/*
Function will return the score of the user
Input: none
Output: the score of the user
*/
int User::getScore()
{
	return this->_score;
}

/*
Function will return the best score of the user
Input: none
Output: the best score of the user
*/
int User::getBestScore()
{
	return this->_bestScore;
}

/*
Function will return the game of the user
Input: none
Output: the game of the user
*/
Game* User::getGame()
{
	return this->_currGame;
}

/*
Function will reset the score of the user
Input: none
Output: none
*/
void User::resetScore()
{
	this->_bestScore = 0;
	this->_score = 0;
}