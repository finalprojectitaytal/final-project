#pragma once
#ifndef USER_H
#define USER_H
#include <string>
#include <WinSock2.h>
#include "Room.h"
#include "Helper.h"
#include "Game.h"

#define MINUS -1

///Done by Itay

class Room;
class Game;

using std::string;

class User
{
public:
	User(string username, SOCKET sock);
	~User();
	void send(string message);
	void setGame(Game* gm);
	void clearGame();
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime,string category);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	void clearRoom();
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	void upScore();
	void setBestScore();
	int getScore();
	int getBestScore();
	Game* getGame();
	void resetScore();

private:
	string _username;
	SOCKET _sock;
	Room* _currRoom;
	Game* _currGame;
	int _score;
	int _bestScore;
};
#endif // !USER_H

