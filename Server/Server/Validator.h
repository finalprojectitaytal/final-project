#pragma once

#include <string>

#define MIN_LENGTH 4
#define SPACE ' '
#define ZERO 0

using std::string;

bool isPasswordValid(string password);
bool isUsernameValid(string username);